.. index::
   pair: Rassemblement ; Contre les executions en iran rassemblement a grenoble (2024-04-27)


.. _iranluttes_2024_04_27:

======================================================================================================================================================================
✊ ⚖️ 📣 2024-04-27 **Contre la peine de mort de Toomaj Salehi rassemblement à grenoble le samedi 27 avril 2024 à 14h30 place Félix Poulat** |toomaj|
======================================================================================================================================================================

- https://blogs.mediapart.fr/bahareh-akrami/blog/240424/toomaj-salehi-rappeur-engage-et-dissident-politique-ete-condamne-mort

#freetoomaj #toomajsalehi #womenlifefreedom #mahsaamini #iranrevolution
#notoexecutionsiniran #notodeathpenalty #irgcterrorists #notoislamicrepublic
#savetoomaj  #RezaRasaei

.. figure:: images/affiche.jpeg


.. figure:: images/dessin_bahare.webp

   https://blogs.mediapart.fr/bahareh-akrami/blog/240424/toomaj-salehi-rappeur-engage-et-dissident-politique-ete-condamne-mort


Iran : il faut sauver le rappeur Toomaj, condamné à la peine de mort
=====================================================================

- https://www.liberation.fr/idees-et-debats/tribunes/iran-il-faut-sauver-le-rappeur-toomaj-condamne-a-la-peine-de-mort-20240426_5W76CMJOLZAIZK4W3VEW3YYNF4/
- https://iranhr.net/en/articles/6663/ (Protester Reza Rasayi at Imminent Risk of Execution.. figure:: images/ 23 Execution Recorded in 5 Days)

.. figure:: images/toomaj.png


Plusieurs organisations appellent à un rassemblement à Paris ce dimanche 28
avril 2024 à 15 heures pour l’abolition de la peine de mort et la libération des
prisonniers d’opinion et politiques iraniens dont le chanteur engagé Toomaj
Salehi.

Plus de dix-huit mois après le début du soulèvement populaire "Femme, vie,
liberté» de 2022, les autorités iraniennes continuent de réprimer encore
davantage les droits à la liberté d’expression, d’association et de réunion
pacifique.

La répression s’intensifie à l’encontre des femmes et des jeunes filles qui
défient les lois sur le port obligatoire du voile.
Le recours à la peine de mort comme instrument de répression politique
s’accroît avec une accélération du nombre d’exécutions.

Au moins sept personnes sont sous le coup d’une condamnation à mort pour avoir
pris part aux manifestations nationales "Femme vie liberté», à la suite de
procès manifestement inéquitables.

Il s’agit de:

- Fazel Bahramian,
- Mamosta Mohammad Khazrnejad,
- Manouchehr Mehman Navaz,
- Mehran Bahramian
- Mojahed (Abbas) Kourkour,
- `Reza (Gholamreza) Rasaei <https://iranhr.net/en/articles/6663/>`_
- le rappeur Toomaj Salehi, dont l’avocat a annoncé le 24 avril 2024
  qu’un tribunal révolutionnaire l’avait condamné à la peine de mort.


.. figure:: images/les_4.webp


Parmi les autres artistes visés par les autorités iraniennes dans le contexte
du soulèvement et de ses suites, on peut citer:

- le rappeur Saman Yasin, arrêté arbitrairement en octobre 2022 puis condamné
  à cinq ans d’emprisonnement,
- et l’actrice Taraneh Alidoosti, détenue arbitrairement en janvier 2023 pour
  avoir publié sur les réseaux sociaux des messages en soutien au mouvement
  "Femme, vie, liberté», sans voile obligatoire.

Arrêté, détenu arbitrairement pour la première fois et emprisonné en
octobre 2022, puis soumis à la torture et à d’autres mauvais traitements,
notamment à un isolement prolongé, Toomaj Salehi a été libéré de prison
sous caution en novembre 2023.
Le même mois, il a de nouveau été arrêté et détenu arbitrairement après avoir
dénoncé publiquement sa détention.

Nous, organisations des droits humains, rappelons que la peine de mort est une
violation du droit à la vie tel qu’il est proclamé dans la Déclaration
universelle des droits de l’homme et constitue le châtiment cruel, inhumain
et dégradant par excellence.

Dans le cadre des manifestations de 2019
============================================

- `Abbas Deris <https://www.amnesty.ch/fr/participer/ecrire-des-lettres/actions-urgentes/annees/2024/ua-088-23-iran>`_


|RezaRazaei| Reza Razaei Protester Reza Rasayi at Imminent Risk of Execution,  23 Execution Recorded in 5 Days
==================================================================================================================

- https://iranhr.net/en/articles/6663

.. figure:: images/reza.png


Abas deris
===========

- https://www.amnesty.ch/fr/participer/ecrire-des-lettres/actions-urgentes/annees/2024/ua-088-23-iran

Le manifestant Abbas Deris risque d’être exécuté sous peu en lien avec les
manifestations qui ont secoué le pays en novembre 2019.

Son avocat a déclaré publiquement le 4 juillet que la Cour suprême avait confirmé
sa déclaration de culpabilité et sa peine capitale pour «inimitié à l’égard de Dieu» (moharebeh).

Fin octobre 2022, un tribunal révolutionnaire à Mahshahr l’a condamné à mort
à l’issue d’un procès manifestement inique entaché d’«aveux» diffusés à la
télévision d’État quelques semaines après son interpellation.

Sa demande de réexamen judiciaire est toujours en instance devant la Cour suprême.


Introduction par Zoya
=========================

.. youtube:: pak9lTYlgZI

Discours Amnesty Grenoble contre la peine de mort de Toomaj Salehi
====================================================================

.. youtube:: kwzM-qevkqs

Discours de Zoya sur la mafia des mollahs en Iran
===================================================

.. youtube:: wtgTWSXUbYs


Au moins sept personnes sont sous le coup d’une condamnation à mort pour avoir
pris part aux manifestations nationales "Femme vie liberté», à la suite de
procès manifestement inéquitables.

Il s’agit de:

- Fazel Bahramian,
- Mamosta Mohammad Khazrnejad,
- Manouchehr Mehman Navaz,
- Mehran Bahramian
- Mojahed (Abbas) Kourkour,
- `Reza (Gholamreza) Rasaei <https://iranhr.net/en/articles/6663/>`_
- le rappeur Toomaj Salehi, dont l’avocat a annoncé le 24 avril 2024
  qu’un tribunal révolutionnaire l’avait condamné à la peine de mort.


Ressource livre : **La face cachée des mollahs** par Emmanuel RAZAVI
----------------------------------------------------------------------------

- :ref:`iran_luttes:mollahs_2024_01`
- https://www.radiofrance.fr/franceculture/podcasts/signes-des-temps/la-republique-islamique-d-iran-quel-avenir-pour-le-regime-8183839



Reportage de France bleu
============================

- https://www.francebleu.fr/infos/international/iran-les-franco-iraniens-de-l-isere-ont-manifeste-a-grenoble-en-soutien-a-toomaj-salehi-le-rappeur-condamne-a-mort-7109798


Quelques photos
==================


.. figure:: images/20240427_145536_800.webp
.. figure:: images/20240427_145542_800.webp
.. figure:: images/20240427_150308_800.webp
.. figure:: images/20240427_150537_800.webp
.. figure:: images/20240427_151208_800.webp
.. figure:: images/20240427_151224_800.webp
.. figure:: images/20240427_151235_800.webp
.. figure:: images/20240427_151245_800.webp
.. figure:: images/20240427_151320_800.webp
.. figure:: images/20240427_151415_800.webp
.. figure:: images/20240427_151620_800.webp
.. figure:: images/20240427_151624_800.webp
.. figure:: images/20240427_152755_800.webp
.. figure:: images/20240427_153830_800.webp
.. figure:: images/20240427_154643_800.webp
.. figure:: images/20240427_155009_800.webp
.. figure:: images/20240427_155013_800.webp
.. figure:: images/20240427_155911_800.webp
.. figure:: images/20240427_160041_800.webp
.. figure:: images/20240427_160325_800.webp
.. figure:: images/20240427_160828_800.webp

