

.. _bito_2024_04_27:

=========================================================================================================================================================
2024-04-27 **Iran : les franco-iraniens de l'Isère ont manifesté à Grenoble en soutien à Toomaj Salehi, le rappeur condamné à mort**  par Romain Bitot
=========================================================================================================================================================

- https://www.francebleu.fr/infos/international/iran-les-franco-iraniens-de-l-isere-ont-manifeste-a-grenoble-en-soutien-a-toomaj-salehi-le-rappeur-condamne-a-mort-7109798


Journaliste : Romain Bitot
============================

- https://www.francebleu.fr/les-equipes/romain-bitot

Iran : les franco-iraniens de l'Isère ont manifesté à Grenoble en soutien à Toomaj Salehi, le rappeur condamné à mort
=========================================================================================================================


.. figure:: images/france_bleue_free_toomaj.webp

Environ 70 personnes, dont des franco-iraniens, ont manifesté samedi 27 avril 2024
place Félix Poulat à Grenoble (Isère) en soutien au rappeur condamné à mort
Toomaj Salehi et au peuple iranien.


Libérez Toomaj, Femmes, vie, liberté !".

Voici les slogans repris par les 70 manifestants rassemblés place Félix Poulat
à Grenoble (Isère) samedi 27 avril.

Parmi eux, des franco-iraniens venus exprimer leur solidarité au rappeur Toomaj Salehi,
condamné à mort le mercredi 24 avril par un tribunal iranien pour avoir soutenu
dans ses morceaux les mouvements de contestation dans le pays.

"C'est quelqu'un de très connu au pays.

Ses chansons, elles sont adressées  au peuple iranien", raconte Calie Loyal,
Iranienne présidente de l'association SolidIran.
"Il a toujours manifesté, il allait avec les enfants dans la rue.
Aujourd'hui, c'est important pour nous de parler de lui".


.. figure:: images/france_bleue_cali.webp

   Calie Loyal, présidente de SolidIran, est venue afficher son soutien à Toomaj Salehi. © Radio France - Romain Bitot


"C'est un cri de désespoir"
================================

Au milieu des affiches "Free Toomaj" et sur fond de morceaux du rappeur
diffusé sur une enceinte, Kaver, arrivé en France il y a neuf ans, ne comprend
toujours pas comment l'artiste a pu être condamné :

"C'est pas possible ! Il fait juste de la musique, c'est tout",

s'énerve-t-il.

Tous les Iraniens réunis sur la place Félix Poulat veulent continuer à se
mobiliser, même s'ils sont loin de leur pays.

Mais pour Dina, ça devient difficile : "C'est un cri de désespoir en fait.
On crie mais je ne suis pas sûre que les gens qui ont du pouvoir nous entendent
là-bas", soupire-t-elle.

"J'espère quand même qu'un jour on pourra voir l'Iran avec une vraie démocratie".

.. figure:: images/france_bleue_pancartes.webp

   Plusieurs pancartes en soutien à Toomaj Salehi ont été déposées place Félix Poulat. © Radio France - Romain Bitot
