.. index::
   ! Iran : il faut sauver le rappeur Toomaj, condamné à la peine de mort

.. _ldh_2024_04_26:

=============================================================================================================================================
2024-04-26 **Iran : il faut sauver le rappeur Toomaj, condamné à la peine de mort** par Un collectif d'organisations des droits humains
=============================================================================================================================================

- https://www.liberation.fr/idees-et-debats/tribunes/iran-il-faut-sauver-le-rappeur-toomaj-condamne-a-la-peine-de-mort-20240426_5W76CMJOLZAIZK4W3VEW3YYNF4/
- https://www.liberation.fr/international/moyen-orient/iran-le-rappeur-contestataire-toomaj-salehi-condamne-a-mort-20240424_7CNDBIYLXNE3NH7WK4K26CQ64E/



Iran : il faut sauver le rappeur Toomaj, condamné à la peine de mort
=====================================================================

- https://iranhr.net/en/articles/6663/ (Protester Reza Rasayi at Imminent Risk of Execution; 23 Execution Recorded in 5 Days)

Plusieurs organisations appellent à un rassemblement à Paris ce dimanche 28
avril 2024 à 15 heures pour l’abolition de la peine de mort et la libération des
prisonniers d’opinion et politiques iraniens dont le chanteur engagé Toomaj
Salehi.

Plus de dix-huit mois après le début du soulèvement populaire "Femme, vie,
liberté» de 2022, les autorités iraniennes continuent de réprimer encore
davantage les droits à la liberté d’expression, d’association et de réunion
pacifique.

La répression s’intensifie à l’encontre des femmes et des jeunes filles qui
défient les lois sur le port obligatoire du voile.
Le recours à la peine de mort comme instrument de répression politique
s’accroît avec une accélération du nombre d’exécutions.

Au moins sept personnes sont sous le coup d’une condamnation à mort pour avoir
pris part aux manifestations nationales "Femme vie liberté», à la suite de
procès manifestement inéquitables.

Il s’agit de:

- Fazel Bahramian,
- Mamosta Mohammad Khazrnejad,
- Manouchehr Mehman Navaz,
- Mehran Bahramian
- Mojahed (Abbas) Kourkour,
- `Reza (Gholamreza) Rasaei <https://iranhr.net/en/articles/6663/>`_
- le rappeur Toomaj Salehi, dont l’avocat a annoncé le 24 avril 2024
  qu’un tribunal révolutionnaire l’avait condamné à la peine de mort.

Parmi les autres artistes visés par les autorités iraniennes dans le contexte
du soulèvement et de ses suites, on peut citer:

- le rappeur Saman Yasin, arrêté arbitrairement en octobre 2022 puis condamné
  à cinq ans d’emprisonnement,
- et l’actrice Taraneh Alidoosti, détenue arbitrairement en janvier 2023 pour
  avoir publié sur les réseaux sociaux des messages en soutien au mouvement
  "Femme, vie, liberté», sans voile obligatoire.

Arrêté, détenu arbitrairement pour la première fois et emprisonné en
octobre 2022, puis soumis à la torture et à d’autres mauvais traitements,
notamment à un isolement prolongé, Toomaj Salehi a été libéré de prison
sous caution en novembre 2023.
Le même mois, il a de nouveau été arrêté et détenu arbitrairement après avoir
dénoncé publiquement sa détention.

Nous, organisations des droits humains, rappelons que la peine de mort est une
violation du droit à la vie tel qu’il est proclamé dans la Déclaration
universelle des droits de l’homme et constitue le châtiment cruel, inhumain
et dégradant par excellence.

Pour un moratoire officiel sur les exécutions
====================================================

Tous les Etats qui maintiennent la peine de mort, dont l’Iran doivent établir un
moratoire officiel sur les exécutions, en vue d’abolir la peine de mort, et en
tant que pays abolitionniste, il revient aux autorités françaises d’intervenir
d’urgence en faisant pression sur les autorités iraniennes afin qu’il soit
mis fin à toutes les exécutions, sans délai.

Pour la seule année 2023, les autorités iraniennes ont procédé à une vague
d’exécutions terrifiante, avec au moins 853 personnes exécutées, dont plus
de la moitié pour des infractions liées à la drogue, en violation du droit
international.

C’est pourquoi nous, organisations des droits humains, appelons à un large
rassemblement de la société civile dimanche 28 avril 2024 à 15 heures sur la
place de la Bastille, à Paris, autour des revendications suivantes :

- Nous demandons la libération immédiate et inconditionnelle de Toomaj Salehi et
  de toutes les personnes reconnues coupables et condamnées à mort, inculpées
  ou faisant l’objet d’une enquête uniquement pour avoir exercé leurs droits
  à la liberté d’expression, d’association et de réunion pacifique.

- Nous demandons que toutes les condamnations et les peines de mort prononcées à
  la suite des manifestations soient annulées, les autorités doivent s’abstenir
  de demander de nouvelles condamnations à mort et veiller à ce que toute personne
  accusée d’une infraction pénale reconnue soit jugée dans le cadre d’une
  procédure conforme aux normes internationales en matière d’équité des procès,
  sans recours à la peine de mort.

- Nous appelons à la libération de tous·tes les prisonnier·es d’opinion en Iran.

Nous rappelons notre opposition absolue à la peine de mort en toutes circonstances
et en tous lieux et exhortons le gouvernement français, fidèle à sa tradition
abolitionniste, à mettre en œuvre tous les instruments diplomatiques à sa
disposition pour que l’Iran ratifie les conventions internationales abolissant
la peine de mort.

Signataires :
===============

- Iran Justice,
- Neda d’Iran,
- Amnesty International,
- Queers & Feminists for Iran Liberation,
- Azadi 4 Iran,
- Collectif Phénix,
- Collectif Alborz,
- Ligue des Femmes Iraniennes pour la démocratie,
- Association pour la défense des droits de l’homme et des revendications
  démocratiques culturelles du peuple azerbaïdjanais Iran Hamava,
- Comité indépendant contre la répression des citoyens iraniens,
- Ligue de défense des droits humains en Iran
- International Community of Iranian Academics,
- Alliance des femmes pour la démocratie, Action
- des chrétiens pour l’abolition de la torture,
- Ligue des droits de l’Homme
- Ensemble contre la peine de mort.
