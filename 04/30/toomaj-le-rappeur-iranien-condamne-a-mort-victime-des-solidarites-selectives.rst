.. index::
   pair: Pierre Haski ; Toomaj, le rappeur iranien condamné à mort, victime des solidarités sélectives (2024-04-30)

.. _haski_2024_04_30:

===================================================================================================================
2024-04-30 **Toomaj, le rappeur iranien condamné à mort, victime des solidarités sélectives** par Pierre Haski
===================================================================================================================


- https://www.radiofrance.fr/franceinter/podcasts/geopolitique/geopolitique-du-mardi-30-avril-2024-7403079


Journaliste Pierre Haski
===========================

- https://www.radiofrance.fr/personnes/pierre-haski


Peut-on se montrer à la fois solidaire de Toomaj Salehi, le rappeur iranien condamné à mort à Téhéran, des Palestiniens bombardés chaque jour à Gaza, et des otages israéliens toujours aux mains du Hamas ? Ou est-on soumis à une injonction à choisir ses victimes depuis le 7 octobre ?
========================================================================================================================================================================================================================================================================================================

- https://www.radiofrance.fr/personnes/marjane-satrapi

Peut-on se montrer à la fois solidaire de Toomaj Salehi, le rappeur iranien
condamné à mort à Téhéran, des Palestiniens bombardés chaque jour à Gaza,
et des otages israéliens toujours aux mains du Hamas ? Ou est-on soumis à une
injonction à choisir ses victimes depuis le 7 octobre ?

Il s’appelle Toomaj Salehi, ou simplement Toomaj, son nom de rappeur.
Il a 33 ans, et, depuis 2021, il a passé plus de temps en prison qu’en liberté.
**Il y a quelques jours, un tribunal iranien l’a condamné à mort, une peine
outrancière pour un musicien contestataire non-violent**.

Toomaj est l’un des visages de cette génération qui s’est solidarisée avec
Mahsa Amini, la jeune femme morte entre les mains de la police des mœurs pour
un voile mal ajusté. Une campagne internationale prend forme pour le sauver de
ses bourreaux et demander sa libération.

Plusieurs personnalités, dont l’artiste `Marjane Satrapi <https://www.radiofrance.fr/personnes/marjane-satrapi>`_
et l’actrice `Golshifteh Farahani <https://www.radiofrance.fr/personnes/golshifteh-farahani>`_, ont écrit hier à Emmanuel Macron pour lui demander d’intervenir en
faveur du musicien, rappelant au président qu’il avait reçu l’an dernier
un groupe de femmes iraniennes et avait soutenu leur combat.

Mais Toomaj est aussi pris, malgré lui, dans les clivages générés par le
conflit israélo-palestinien, qui poussent à choisir ses victimes

Notre monde a des émotions sélectives, et accepte des concurrences victimaires
dérangeantes. Ceux qui se mobilisent pour Toomaj -il y a eu des rassemblements
dans plusieurs pays dont la France dimanche- déplorent de ne pas y voir ceux
qui protestent avec véhémence contre les massacres de Gaza. Peut-être parce
que l’Iran est en confrontation ouverte avec Israël, et, par un travers de la
pensée, soutenir un adversaire du régime de Téhéran serait jouer le jeu de
ceux qui bombardent Gaza.

Il y a dans ce Proche et Moyen Orient traversé par des passions et des vents
de folie, une empathie sélective qui a contaminé le reste du monde.

Depuis l’attaque du Hamas le 7 octobre 2023 en Israël, qui a fait plus de mille morts
dont une majorité de civils, et la riposte israélienne qui a fait des dizaines
de milliers de victimes dans la bande de Gaza, là aussi une majorité de civils,
**il y a une injonction à choisir ses morts**.

**La solidarité est devenue sélective**, comme si on ne pouvait pas défendre des
principes et regretter toutes les victimes.

**Le sort de ce rappeur condamné à mort devrait rassembler toutes les solidarités**

Le mouvement "femmes, vie, liberté" né de la mort de Mahsa Amini avait
soulevé soutien et admiration dans une bonne partie du monde ; la défense de
Toomaj devrait en être le prolongement.

**Mais les conflits de la région sont passés par là. Le régime iranien tente de
surfer sur les mouvements de solidarité avec les Palestiniens pour se refaire
une virginité ; et profite du fait que les campus occidentaux sont focalisés
sur Gaza et ignorent les autres causes**.

Et pourtant, une défense des droits humains et tout simplement de valeurs
humanistes devrait permettre de se dire solidaire, dans le même souffle et sans
réserve:

- des Palestiniens qui subissent un châtiment collectif insupportable dans
  la bande de Gaza ;
- des otages israéliens toujours retenus par le Hamas, ce qui constitue là
  aussi un crime de guerre ;
- et d’un rappeur iranien épris de liberté et qui risque d’être exécuté par
  un régime impitoyable.

Force est de constater que c’est hélas impossible en 2024.


Liens
======

- https://www.radiofrance.fr/franceinter/le-realisateur-iranien-saeed-roustaee-condamne-a-six-mois-de-prison-pour-avoir-diffuse-un-film-a-cannes-4721113
- https://www.radiofrance.fr/franceinter/podcasts/on-va-en-reparler/on-va-en-reparler-du-dimanche-10-septembre-2023-5204052
- https://www.radiofrance.fr/franceinter/podcasts/la-bo-du-monde/la-bo-du-monde-du-lundi-04-decembre-2023-2195160
