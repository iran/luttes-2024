.. index::
   ! Iran-Israël : "L’attaque s’est terminée en partie d’échecs dont tout le monde est sorti gagnant (Denis Charbit, 2024-04-25)

.. _charbit_2024_04_25:

===========================================================================================================================================
2024-04-25 **Iran-Israël : "L’attaque s’est terminée en partie d’échecs dont tout le monde est sorti gagnant"** par Denis Charbit
===========================================================================================================================================

- https://www.lemonde.fr/idees/article/2024/04/25/iran-israel-l-attaque-s-est-terminee-en-partie-d-echecs-dont-tout-le-monde-est-sorti-gagnant_6229886_3232.html

Introduction
==============

Si les deux acteurs se sont mutuellement adressé une leçon sans trop de dégâts,
ni directs ni collatéraux, il n’en reste pas moins que le territoire israélien
n’est plus un sanctuaire, analyse le politiste

Denis Charbit


Le lundi 22 avril au soir, les rues d’Israël étaient vides
===============================================================

Le lundi 22 avril au soir, les rues d’Israël étaient vides.

Non, les Israéliens n’ont pas passé la soirée dans les abris comme ils l’ont
fait si souvent depuis le 7 octobre 2023 et, tout récemment encore, le 13 avril,
lorsque les sirènes ont déchiré le silence pour avertir de l’attaque iranienne
sur le pays.
**Ils ont célébré la sortie d’Egypte, le passage de la servitude à la liberté.**

Cette année, l’ambiance fut pesante et retenue, aux antipodes de la cacophonie
des soirées de Pâque ordinaires.
Dans de nombreux foyers, une chaise vide était placée autour de la table pascale
en signe de solidarité avec les 133 otages toujours retenus par le Hamas, sans
que rien filtre sur leur sort.

C’est, à ce jour, le sujet de discorde le plus vif : la morale du contrat social
contre celle de la raison d’Etat.

- En vertu du premier, Benyamin Nétanyahou a violé une règle non écrite du devoir
  de l’Etat envers ses citoyens en refusant de faire de la libération des otages
  une priorité ;
- en vertu de la seconde, pour mener à bien l’objectif de guerre, le président
  israélien peut avoir d’autres préoccupations que celle de négocier un accord
  avec le Hamas.

Et l’attaque iranienne dans tout ça ?
==========================================

Et l’attaque iranienne dans tout ça ? Oubliée.

Une semaine et demie après la nuit blanche éclairée par l’interception israélienne
des drones et missiles iraniens, l’attaque, qui avait commencé comme la
chronique d’une tragédie annoncée, s’est terminée en partie d’échecs dont
tout le monde est sorti gagnant.

Le 13 avril est apparu initialement comme l’anti-7 octobre
===========================================================

L’armée a démontré ses capacités à la perfection.

Si seulement elle avait agi avec la même maestria il y a deux cents jours…

Dépourvu d’armée de l’air et de mer, le Hamas pénétrait en territoire israélien
et montait sur place un théâtre de la cruauté inégalé.

**Côté israélien, savoir-faire, technologie, renseignement, entraînement, tout
s’est écroulé comme un château de cartes deux-trois jours durant, deux-trois
jours de trop**.

La résilience, une seconde nature
==========================================

Depuis lors, l’armée dans Gaza piétine tout en avançant ; elle s’efforce de
rapporter une victoire, elle n’y parvient pas.

Le Hamas plie mais ne se rend pas. Les pertes humaines palestiniennes sont
considérables.

Pour soutenir la légitimité d’Israël à mener une expédition, ses alliés peinent
à comprendre la conduite des opérations.
L’ONU condamne, les passions militantes s’enflamment, la Cour internationale
de justice adresse un blâme.
Le "risque de génocide" est perçu comme une inadmissible offense par le peuple
qui l’a éprouvé et qui sait dans sa chair ce que le mot signifie.

La défense israélienne, qui s’est effondrée en octobre, a fait, en avril, un
sans-faute.
Israël, souvent accusé de faire de la force un usage disproportionné, a réagi,
cette fois, par une riposte calibrée, donnant un avertissement qui était simultanément
un signe d’accalmie.

Une communication tacite existe toujours entre ennemis jurés.

Chacun a joué sa partition, sans fausse note.

Les deux acteurs qu’on soupçonne généralement d’être irrationnels – la "mollahcratie"
à cause de sa foi ardente et conquérante, et Nétanyahou dont toute la conduite
ne serait inspirée que par la volonté de rester au pouvoir – se sont mutuellement
adressé une leçon sans causer trop de dégâts, ni directs ni collatéraux :
Israël a commencé, l’Iran a riposté, Israël a fermé le ban.

Informés à temps par les Iraniens, Saoudiens et Jordaniens ont prévenu leur
allié israélien. La Russie a calmé le jeu. Les Chinois ont choisi la retenue,
tandis que les Américains, les Britanniques et les Français ont volé au secours
d’Israël et que les proxys sont restés à l’écart.

**La séquence mériterait de devenir un morceau d’anthologie en stratégie et
relations internationales**.

Il n’en reste pas moins que la dissuasion a failli à deux reprises : le territoire israélien n’est plus un sanctuaire
=======================================================================================================================

Depuis 1948, les Israéliens ont éprouvé sept guerres, deux Intifadas, deux
attaques de missiles d’Irak et d’Iran : ils ont mené des opérations par dizaines
et subi des attentats par milliers.
Les menaces sur le pays sont intermittentes, puis aiguës, puis latentes.
Comment tenir, sinon en développant une résilience qui devient une seconde nature ?

Calendrier politique
========================

S’il est absurde de penser qu’Israël n’a pas sa part de responsabilité dans
le conflit, il est tout aussi stupide de croire que ces menaces résultent
d’un contexte qu’Israël serait le seul à nourrir.

Outre l’occupation dont trop d’Israéliens, drapés dans leur dogme nationaliste,
nient le noyau moral qui légitime la lutte menée contre elle, il faut comprendre
aussi qu’à ne voir Israël que sous l’angle colonial, en niant et ignorant
tout ce qui ne colle pas avec cette étiquette, on légitime d’avance un grand
charnier sur lequel on immolera les Israéliens colonisateurs, et dont le 7
octobre a été une préfiguration effroyable.

**La solitude d’Israël doit aussi à l’impéritie d’un leadership gonflé de
certitudes, mais dépourvu de la moindre vision d’avenir** susceptible de mettre
fin à cette danse macabre où Palestiniens et Israéliens sont embarqués.

En attendant, on voit que les pays signataires des accords d’Abraham ou
d’accords trilatéraux (Etats-Unis, Maroc et Israël) ont résisté à la
pression qui les exhortait à les rompre.

On a prétendu que le rapprochement saoudien avec Israël était compromis, sinon
enterré.
Si, avant le 7 octobre, Mohammed Ben Salman était susceptible de nouer des
relations avec Israël sans rien obtenir que des concessions purement symboliques
en faveur des Palestiniens, il pourrait maintenant jouer un triple rôle :

- être le bailleur de fonds de la reconstruction de Gaza,
- le leader d’une force d’interposition arabe chargée d’assurer la sécurité
  de la bande,
- et le médiateur, enfin, d’une négociation israélo-palestinienne rétablie
  après dix ans d’interruption.

La bataille de Rafah, toujours repoussée, devrait être un baroud d’honneur,
rien de plus.
C’est alors que le calendrier sera politique : élections en Israël et renvoi
de Benyamin Nétanyahou, remaniement de fond au sein de l’Autorité palestinienne
et réélection, en novembre, de Joe Biden.

Le 7 octobre aura un impact, à condition qu’il ne profite pas au Hamas.


"Am Israel Hai", "Palestine vivra" aux côtés d’Israël, voilà de quoi entrevoir  le jour au bout du tunnel
============================================================================================================

"Am Israel Hai", déclarent les Israéliens, malgré les contusions et les
blessures qu’ils endurent et les coups qu’ils infligent ;

"Palestine vivra", clament leurs adversaires.

Les premiers s’inquiètent de leur survie et ils n’ont pas tort ;

les seconds s’inquiètent de ce qui reste de leur terre et ils ont raison.

"Am Israel Hai", "Palestine vivra", voilà deux slogans qui valent bien mieux
que le "From the river to the sea" qui, en hébreu, est une réalité funeste
pour les Palestiniens et, en arabe, un cauchemar tout aussi fatal pour les
Israéliens.

"Am Israel Hai", "Palestine vivra" aux côtés d’Israël, voilà de quoi entrevoir
le jour au bout du tunnel.
