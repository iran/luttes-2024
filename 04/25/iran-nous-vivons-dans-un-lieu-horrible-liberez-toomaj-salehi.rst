

.. _fa_2024_04_26:

====================================================================================================
2024-04-26 **Urgence Iran : 'nous vivons dans un lieu horrible' libérez Toomaj Salehi** par FA
====================================================================================================

- https://federation-anarchiste.org/?g=Lien_Permanent&b=1_246

::

    "Nous nous lèverons jusqu’en haut de la pyramide".
    "Ici, les gens sont seulement vivants. Ils n’ont pas de vie".

(Textes de Toomaj Salehi, 33 ans, rappeur, condamné à mort en Iran en avril 2024)

Déjà arrêté et condamné à six mois de prison pour "diffusion de propagande
contre l’État" en septembre 2021 en raison de ses textes contre le régime
théocratique islamiste, puis arrêté en octobre 2022 pour "incitation à
la sédition" lors de la révolte "Femme, vie, liberté", Toomaj Salehi
est finalement condamné à six ans et trois mois de prison en juillet 2023 avec
interdiction de pratiquer la musique.

L’artiste est à nouveau arrêté peu après sa sortie de prison en novembre 2023
et vient d’être condamné à mort le 24 avril 2024 pour **"corruption sur Terre"**.

Il avait déclaré, comme Bakounine::

    "Je pensais que la situation la plus triste pour une personne était d’être
    seule sous la torture, maintenant je comprends qu’être seul à être libéré
    tandis que les autres sont toujours en prison, est encore plus amer".

En novembre 2023, Toomaj avait pu rencontrer la militante contre le port du
voile obligatoire, Sepideh Rashnu, qui doit purger une peine de quatre ans de
prison.

Puis, à la prison d’Evin (Téhéran), en janvier de cette année, il s’était joint
à la grève de la faim de Narges Mohammadi, militante pour les droits humains,
prix Nobel de la paix 2023, condamnée pour avoir dénoncé la dictature religieuse
et ses oppressions à l’encontre des femmes.

Le 24 avril 2024 au soir, une banderole représentant Toomaj Salehi a été suspendue
d’un pont de l’autoroute Modares - Téhéran pour protester contre le verdict.

L’art peut être un vecteur formidable d’expression et de résistance face à
la répression, le pouvoir essaye vainement de faire taire Toomaj en le condamnant
à mort, ses chansons continueront à vivre et à rassembler !

Contre les exécutions, toujours plus nombreuses en Iran, pour la libération
de Toomaj et de toutes les personnes qui payent de leur liberté et de leur vie,
leur militantisme pour la vie et la liberté de toutes et tous,

DIFFUSONS L’APPEL A SOLIDARITE DE LA FEDERATION ANARCHISTE.
MOBILISONS-NOUS PARTOUT CONTRE L’EXECUTION DE TOOMAJ SALEHI !



Relations Internationales de la Fédération anarchiste
