

.. _mohammadi_2024_04_25:

=================================================================================================
2024-04-25 **Message de la prix Nobel de la paix #NargesMohammadi depuis la prison d’Evin**
=================================================================================================


Message de la prix Nobel de la paix #NargesMohammadi depuis la prison d’Evin

Les mains puissantes, fermes et unies du peuple iranien ne permettront pas
que le régime en place pende #ToomajSalehi par la corde.

#ToomajSalehi est la voix du mouvement "Femme, Vie, Liberté" et sa musique
est son hymne.

L'exécution de Toomaj est l'exécution de notre mouvement vivant.

Pour que notre mouvement reste en vie, levons-nous.

Levons-nous tous ensemble.
Soulevons-nous contre l'exécution de Toomaj,
Soulevons-nous pour les femmes,
Soulevons-nous pour la vie,
Soulevons-nous pour la liberté.

Les exécutions vengeresses, lâches et cruelles, sont une tache de honte sur
le vêtement sombre et corrompu du régime théocratique et despotique.

Ne permettons pas qu'une autre goutte de sang jeune tache les terres blanches
de notre pays.
Nous le pouvons.

Le régime clérical tyrannique doit savoir que, en émettant des jugements cruels
et vengeurs contre des enfants fiers de l'Iran comme #ToomajSalehi et en
luttant contre les femmes iraniennes, il verra la colère et la résistance à
l'oppression du peuple iranien.


Narges Mohammadi
Evin
