

.. _mohammadi_2024_04_27:

========================================================================================================================
2024-04-25 **Toomaj Salehi, is the voice of “Woman, Life, Freedom” movement & its soundtrack** by Narges Mohammadi
========================================================================================================================


A message from Narges Mohammadi for Toomaj Salehi’s freedom.


.. figure:: images/toomaj.webp

WE MUST RISE UP !
======================

Toomaj Salehi, is the voice of “Woman, Life, Freedom” movement & its soundtrack.
The execution of Toomaj is the death of our movement.

To keep our movement alive we must rise up TOGETHER.

Rise Against the execution of Toomaj,
Rise for women,
Rise for life,
Rise for freedom.
Vengeful, cowardly, and oppressive executions are a shameful stain on the foul
and dark garment of the authoritarian religious government.

Let us not allow the blood of another young person to mar the pure expanse of our land.
We can prevent this!

The Islamic Republic must understand that by issuing unjust sentences against
honorable children of this country like Toomaj Salehi, & by waging war against
women, it will face the Iranian people’s uprising against oppression & injustice.”



Narges Mohammadi
Evin Prison
April 25, 2024
