.. index::
   ! Des prisons en Iran Laboratoire de cruauté et matrice de la violence d’État contre une contestation sociale et politique sans précédent (2024-12-09)
   pair: Fariba Adelkhah; Réponse (2024-12-09)

.. _prisons_iran_2024_12_09:

========================================================================================================================================================================
2024-12-09 **Des prisons en Iran Laboratoire de cruauté et matrice de la violence d’État contre une contestation sociale et politique sans précédent** par Collectif
========================================================================================================================================================================

- https://lmsi.net/Les-prisons-en-Iran-laboratoire-de-cruaute-et-matrice-de-la-violence-d-Etat


P.-S. Crédits Photo : Prison d’Evin, Ehsan Iran, 2008 (copyright : CC BY-SA 2.0)
====================================================================================


.. figure:: images/prison_evin.webp

Préambule
==============

Suite à de nombreuses interventions dans les médias de Fariba Adelkhah, 
anthropologue et ancienne prisonnière en Iran, autrice du livre Prisonnière 
à Téhéran (Seuil, 2024), un collectif d’auteur·ice·s (liste complète en post-scriptum) 
a estimé utile de rappeler certains faits largement documentés, mais omis 
ou niés, concernant les prisons et plus largement la situation des droits 
humains en Iran. 

En vertu de la loi en la matière, nous publions à la suite de cet article 
(après la liste des signataires) un "droit de réponse" de Fariba Adelkhah 
qui nous a été adressé dimanche 22 décembre 2024, en laissant les lecteurs 
et lectrices juges de sa pertinence au regard du texte incriminé. 

Nous y joignons toutefois une brève mise au point des auteur·ice·es de la 
tribune, ainsi qu’un commentaire de notre collectif, mis en cause publiquement 
de manière injurieuse et mensongère par Jean-François Bayart, préfacier du 
livre de Fariba Adelkhah.


Les conditions d’incarcération ont changé en Iran depuis les années 1980,
où plusieurs milliers de prisonniers politiques furent exécutés – parfois
collectivement. Mais les prisons, notamment celles d’Évin, demeurent des
institutions totales où s’exercent l’arbitraire et la violence d’un
régime répressif. 
Elles sont loin d’être "un extraordinaire plateforme
de changement" qu’il conviendrait de "démystifier" et un "lieu de
rencontre et dialogue entre prisonniers et les gens avec lesquels ils vivent"
(autrement dit, les geôliers), comme l’a déclaré l’anthropologue Fariba
Adelkhah, qui a aussi expliqué que, dans la prison d’Evin, "les vrais
geôliers sont les co-détenues." [1]

Contrairement à ces propos, des centaines de témoignages recueillis par les
organisations internationales (ONU, Amnesty International) confirment que la
répression et la surveillance se sont intensifiées depuis le soulèvement
Femme Vie Liberté qui a suivi l’assassinat de Jina Mahsa Amini le 16
septembre 2022 par la police des mœurs pour un voile mal-ajusté. Plusieurs
centaines de personnes ont été tuées et au moins 20,000 personnes arrêtées
et emprisonnées, dans un vaste réseau de centres de détention officiels et
secrets. Une trentaine de jeunes hommes arrêtés lors de ce mouvement, notamment
issus des minorités kurdes et baloutches, ont été exécutés. Une cinquantaine
de prisonniers, dont deux femmes, sont actuellement condamnés à mort.

Amnesty International a recueilli 45 témoignages de viols de femmes, d’hommes
et d’enfants par des agents du Sepah (les gardiens de la révolution) et
de différentes forces de sécurité. Plusieurs de ces victimes, et plusieurs
autres détenus parfois mineurs se sont suicidés après leur libération. Les
tortures physiques et psychologiques – la Torture blanche (Albin Michel,
2024) à laquelle la prix Nobel de la Paix Nargues Mohammadi a consacré un
livre sur la base d’entretiens avec des dizaines de détenues – demeurent des
pratiques courantes. Les refus de soins médicaux, dont la militante et d’autres
prisonnières font l’objet, constituent aussi une forme de violence punitive.

En février 2023, sept prisonnières à Evin alertent sur "les pressions
psychologiques et physiques" qui s’exercent sur les prisonnières, parfois
pendant des années, "dans des cellules d’isolement du quartier de sécurité,
afin qu’elles avouent ce qu’elles n’ont pas fait et que leurs interrogateurs
puissent justifier leur exécution". En août 2024, des prisonnières ont
été violemment battues alors qu’elles "protestaient contre l’exécution,
en secret et à l’aube, d’un manifestant arrêté en 2023 et condamné suite
à des aveux forcés obtenus sous la torture".

Les chercheurs sont tenus par un devoir de vérité, de rigueur, de
non-falsification des faits. Leurs interprétations, le choix de leurs sujets et
leurs affinités politiques ne regardent qu’elles et eux, mais l’intégrité
factuelle de leur travail engage leur profession, et la confiance qu’une
société peut avoir en la parole de celles et ceux qui se revendiquent de
l’observation empirique et de la production de savoirs.

La complexité des relations qui s’instaurent entre des êtres soumis à la
terreur et aux privations dans les prisons de la République islamique a été
analysée dans toute une "littérature des prisons" depuis les années 1980
jusqu’à ce jour. Sans faire un portrait en noir et blanc, sans omettre les
dilemmes et les problèmes, sans peindre la résistance en rose, ces écrits
nombreux n’en disent pas moins clairement où se situe la répression : du
côté de ceux qui enferment et exécutent.

Par ailleurs, de nombreux travaux à travers le monde montrent comment la
suspicion et la mésentente sont les effets concrets d’un pouvoir autoritaire
sur celles et ceux qui le subissent, mais aussi une arme de ce pouvoir pour
continuer son emprise au-delà des frontières où il règne en maître, à
travers des discours de déni et de soupçon qui font son jeu.

La vague de répression inquiétante qui frappe à nouveau la jeunesse en Iran
est destinée à étouffer le rejet politique massif de la République islamique
tenu pour une "dictature" comme l’affirment les slogans dans la rue.

C’est en rejet de ce régime que des femmes retirent leur voile obligatoire,
symbole de l’idéologie d’État, revendiquant la liberté, la laïcité, la
démocratie et la justice sociale. Elles sont loin d’être des "solo riders"
aiguillées par une superficielle soif de célébrité : c’est pour prendre le
contrôle de leur destin, y compris de leur corps et de leur sexualité, qu’elles
transgressent les normes islamiques. Par le biais d’actes performatifs, elles
refusent de dissimuler leurs corps désormais indociles. Elles tentent ainsi
d’échapper au pouvoir disciplinaire d’un régime de surveillance.

Malgré une répression brutale aux mille visages, et une précarité économique
galopante, elles chantent, dansent et proclament l’urgence de la liberté. Car
"le pouvoir exige des corps tristes. Le pouvoir a besoin de tristesse parce
qu’il peut la dominer", comme le rappelait Gilles Deleuze, "la joie,
par conséquent, est résistance".

Le mouvement Femme Vie Liberté illustre l’imbrication des rapports sociaux
de genre, de religion, de sexualité, d’ethnicité, de classe sociale dans un
pays multiethnique et multireligieux qu’est l’Iran. 
Le véritable changement se trouve non dans un adoucissement de l’appareil 
répressif du régime mais dans la société iranienne moderne qui rejette, dans 
sa grande majorité, l’islam politique et la République islamique qui l’incarne.



Signataires 
====================

- Azadeh Kian, professeure de sociologie, directrice du Cedref, Université Paris
  Cité ; 
- Chowra Makaremi, chargée de recherche CNRS, Laboratoire d’Anthropologie
  Politique (EHESS) ; 
- Farhad Khosrokhavar, directeur d’études émérite, EHESS ;
- Marie Ladier-Fouladi, directrice de recherche émérite CNRS, EHESS ; 
- Stéphane Dudoignon, directeur de recherche CNRS, Cetobac, EHESS ; 
- Saeed Paivandi, professeur de sociologie, Université de Lorraine ; 
- Pinar Selek, écrivaine, maîtresse de conférence en sociologie, 
  Université de Côte d’Azur ; Mehran Mostafavi, professeur de chimie physique, 
  vice-président recherche de l’université Paris -Saclay ; 
- Guissou Jahangiri, secrétaire générale de la Fédération
  internationale pour les droits humains (FIDH) et directrice exécutive d’Open
  Asia ; 
- Chirinne Ardakani, avocate, présidente de l’ONG Iran justice ;
- Marjan Satrapi, artiste-écrivaine ; 
- Maryse Artiguelong, vice-présidente de la FIDH ; 
- Maryam Claren, spécialiste de l’Iran à Hawar.Help, fille de la
  prisonnière politique Nahid Taghavi ; 
- Christine Villeneuve, juriste, éditrice, directrice des éditions des femmes ; 
- Elisabeth Nicoli, Avocate, co-présidente de l’Alliance des Femmes pour la 
  Démocratie ; 
- Nathalie Tehio, présidente de la Ligue Française des Droits de l’Homme (LDH).

"Droit de réponse" de Mme Fariba Adelkah à LMSI 
=====================================================


L’honnêteté scientifique et politique dont vous vous réclamez à juste
titre ne devrait pas vous conduire à critiquer un livre pour ce qu’il n’est
pas et n’a jamais prétendu être. Je n’ai jamais voulu écrire un ouvrage
scientifique ou exhaustif sur la prison ou la répression en Iran, mais un
témoignage anthropologique sur ma prison, sur ma privation de liberté pendant
quatre ans et demi. Il ne s’agit de rien d’autre que du récit de choses
vues et vécues par une prisonnière, au demeurant chercheuse et arrêtée en
tant que telle, qui a gardé pendant sa détention son regard d’anthropologue.

Il est d’ailleurs notable que – sauf erreur de ma part – aucun(e) d’entre
vous n’a eu d’expérience carcérale durable en Iran, sinon Stéphane
Dudoignon. Je ne nie en rien les faits que vous rapportez. J’apporte des
éléments complémentaires dont j’espère qu’ils pourront nourrir le livre
de sciences sociales qui reste à écrire sur le système carcéral iranien
et, au-delà, la "littérature des prisons" que vous évoquez, même si
son intention première n’était pas celle-ci, mais plutôt d’informer les
personnes et les institutions qui m’ont soutenue pendant cette épreuve et de
les en remercier. J’estime ne pas être la plus mal placée pour parler de la
prison, ou peut-être plutôt des prisons en Iran, du point de vue des pratiques
sociales qui les constituent. Est-il bien acceptable de vouloir analyser une
institution sociale, notamment du point de vue de l’anthropologie, sans travail
de terrain, celui-ci fût-il involontaire ? Je ne le pense pas, contrairement
à vous, apparemment.

Je tiens par ailleurs à préciser que j’ai résisté, par ma grève de la faim
et en signant une pétition contre les condamnations à mort de militant(e)s du
mouvement Femme, Vie, Liberté, contre la violence de l’Etat iranien. Je l’ai
fait de la prison d’Evin, et non dans le confort de mon appartement. Donc non
sans prendre quelque risque pour défendre mes principes. Comme je l’avais
fait auparavant, notamment en 2009, quand j’étais libre.

Oui, "les mots sont importants", pour reprendre le nom de votre site. Ceux que
l’on devrait lire, plutôt que d’autres qui n’ont pas été écrits. Ceux
que l’on écrit aussi, et que l’on devrait peser quand ils comportent des
accusations aussi graves que celles que vous formulez à mon encontre.

Dans la mesure où votre texte met gravement en cause mon intégrité
professionnelle et morale sur la base de citations soigneusement choisies à charge
et extraites de manière arbitraire du reste de mon livre ou d’interviews,
je vous mets en demeure de publier, au titre du droit de réponse, ma mise au
point dans son intégralité, et je vous en remercie par avance.

Mise au point des rédacteur•ice·s de La Tribune 
=====================================================

- https://www.en-attendant-nadeau.fr/2024/12/17/nous-etions-aussi-rebelles-lune-que-lautre-iran/
- https://www.en-attendant-nadeau.fr/2024/12/17/nous-etions-aussi-rebelles-lune-que-lautre-iran/#:~:text=au%20regard%20de%20tous%20ces%20recits%2C%20le%20temoignage%20de%20la%20chercheuse%20franco-iranienne%20fariba%20adelkhah%2C%20prisonniere%20a%20teheran%2C%20a%20quelque%20chose%20de%20lunaire

Nous souhaitons rappeler que nous n’avons pas réagi au livre de Fariba AdelkhaH
mais **à ses déclarations publiques réitérées dans plusieurs entretiens,
et entendues par plusieurs dizaines de milliers d’auditeurs et auditrices**, qui
n’ont pas tou.te.s lu son livre. 

Notre tribune commence par cette précision. 

**La réponse de Mme Adelkhah devrait donc légitimement concerner ces propos tenus
en entretien, et non le contenu de son livre**.

Nous n’ignorons pas que le contenu d’un ouvrage ne se discute pas dans
une tribune. Une telle discussion appelle la forme de la recension, prenant
davantage le temps de l’analyse. Le livre de Mme Adelkhah a fait l’objet
d’une telle recension par Catherine Coquio, publiée dans la revue En attendant
Nadeau le 17 décembre 2024, et disponible en accès libre et intégral `ici <https://www.en-attendant-nadeau.fr/2024/12/17/nous-etions-aussi-rebelles-lune-que-lautre-iran/#:~:text=au%20regard%20de%20tous%20ces%20recits%2C%20le%20temoignage%20de%20la%20chercheuse%20franco-iranienne%20fariba%20adelkhah%2C%20prisonniere%20a%20teheran%2C%20a%20quelque%20chose%20de%20lunaire>`_

Nous vous invitons à lire cet article rigoureux et précis.

Mise au point du Collectif Les mots sont importants 
========================================================

Au regard de l’émoi qu’ont suscité, autour de nous, les propos évoqués
dans cette tribune, au regard de la mise au point de leurs rédacteur·ice·s,
au regard enfin de la virulence qui a accompagné la publication de ce droit de
réponse en dehors de notre site, sur les médias sociaux, notamment sur le blog
personnel de Jean-François Bayart, préfacier du livre de Fariba Adelkhah, le
Collectif Les mots sont importants tient à rappeler quelques éléments factuels.

Au moment où nous préparons la mise en ligne du droit de réponse de
Fariba Adelkhah, nous découvrons avec stupéfaction qu’il circule
depuis plusieurs jours et qu’il est notamment publié à la date
du 20 décembre sur le blog personnel de Jean-François Bayart (ici :
https://blogs.mediapart.fr/jean-francois-bayart), accompagné de propos injurieux
et mensongers.

Tout d’abord, les signataires de la tribune sont nommément stigmatisés et
disqualifiés par des amalgames que chacun de leurs CV dément : "Vous avez
aimé les attaques de Florence Bergeaud-Blackler contre l’islamo-gauchisme
? Vous adorerez la charge d’Azadeh Kian, Chowra Makaremi, Farhad Khosrokhavar,
Marie Ladier-Fouladi, Stéphane Dudoignon, Saeed Paivandi et – ô tristesse
! – Pinar Selek". 

Plus grossier encore : Jean-François Bayart se contente de qualifier la tribune 
collective de "campagne abjecte", et de reconnaître ironiquement à ses 
signataires la "liberté" de "s’associer à la campagne des néoconservateurs 
étatsuniens et des trumpistes contre la République islamique, qui bénéficient 
de nombreux relais en France, à commencer par l’inoxydable BHL.".

Nous tenons face à ces injures à redire notre solidarité avec les signataires,
tout en reconnaissant à M. Bayart la liberté qu’il a choisi d’exercer :
celle d’user des ficelles rhétoriques "campistes" les plus grossières
et étouffantes pour le débat public.

Nous tenons enfin, puisque c’est la rigueur intellectuelle et le "savoir-lire"
qui sont invoqués par M. Bayart, à signaler et réfuter les contre-vérités
alléguées sur le collectif LMSI. Qualifier sans argumenter l’"orientation
idéologique" du site de "confuse" est là encore un droit (on se contentera
de signaler que le soutien aux luttes d’émancipation des peuples français et
palestinien et syrien, et arménien, et kurde, et turc, et iranien, et ukrainien,
n’a pour nous rien de spécialement confus ou contradictoire, et nous paraît
même suivre une boussole plutôt simple à identifier). 

Il est en revanche mensonger, tout simplement, de prétendre que "LMSI se refuse de publier"
le "droit de réponse" de Mme Adelkhah, "jusqu’à preuve du contraire, au mépris 
de la loi.". 
Ou c’est a minima un parti-pris méthodologique tout à fait singulier, et un 
usage tout sauf rigoureux et "scientifique" de la charge de la preuve, les 
faits étant ceux-ci :

- Au moment où M. Bayart met en ligne ces allégations (le 20 décembre),
  nous ne nous sommes "refusés" à aucune publication, et pour cause :
  aucune demande ne nous est parvenue, ni sur la boite mail du site, ni sur la
  boite personnelle du responsable de la rédaction, Pierre Tevanian ;

- Cette demande nous parvient, sur la boite personnelle de Pierre Tevanian,
  dimanche 22 décembre à 19h08, sans toutefois que le texte dudit "droit de
  réponse" soit joint ;

- Le destinataire en prend connaissance trois heures plus tard et y répond
  immédiatement (à 22H03), en indiquant clairement que le principe des publications
  de droit de réponse ne pose pas de problème, et en invitant donc Mme Adelkhah
  à nous communiquer ce "droit de réponse"

- Le texte nous est adressé à 22H53, et sa mise en ligne est réalisée dès
  le lendemain, 23 décembre.

Les leçons de rigueur et de savoir-lire nous paraissent donc malvenues.
