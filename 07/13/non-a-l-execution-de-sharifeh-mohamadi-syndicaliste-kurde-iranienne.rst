.. index::
   ! Commémoration du premier anniversaire de l’exécution des trois jeunes innocents par la République islamique d’Iran "Majid Kazemi, Saeed Yaghoubi, Saleh Mirhashemi" (2024-05-18)

.. _iranluttes_2024_07_13:

==========================================================================================================================================================
2024-07-13 ⚖️ 📣 à Grenoble, place Félix Poulat à 14h30 **Non à l'exécution de Sharifeh Mohamadi syndicaliste kurde iranienne** |SharifehMohammadi|
==========================================================================================================================================================

Appel
========

Le régime iranien vient de condamner Mme Sharifeh Mohamadi Syndicaliste kurde 
à la peine capitale. 

Elle a été arrêtée en décembre 2023 et condamnée par le tribunal révolutionnaire 
de Rasht (ville située au nord) pour rébellion armée contre le régime 
théocratique le jeudi 4 juillet 2024.

Nous vous invitons à un rassemblement pour dénoncer la férocité de la
répression contre les dissidents, les militants syndicaux et la jeunesse
contestataire et demander l’annulation de cette sentence infondée et
inhumaine et la libération inconditionnelle de madame Sharifeh Mohammadi.

À l’appel de
--------------

- LDH Grenoble Métropole LDH Iran, 
- CISEM, 
- Iran Solidarité, 
- Aiak, 
- SolidIran

Avec le soutien de 
---------------------

- CGT spectacle, 
- NPA anti- capitaliste, 
- Solidaire....

.. figure:: images/affiche.webp


Communiqué des syndicats français: **Empêchons l'exécution de Sharifeh Mohammadi défenseuse des droits citoyens et du travail !**
=============================================================================================================================================

:download:`Communiqué au format PDF <pdfs/communique_syndicat_2024_07_12.pdf>`


Le 4 juillet 2024, Sharifeh Mohammadi |SharifehMohammadi| a été condamnée à mort. 
Son seul "crime" est de défendre les droits des citoyen-nes, citoyen nes, 
ainsi que d'avoir aidé il y a une dizaine d'années à la création d'organisations 
ouvrières indépendantes du pouvoir en place.

Derrière les barreaux depuis le 5 décembre 2023, elle a été depuis soumise 
à de multiples mauvais traitements:

- maintien à l'isolement pendant plusieurs mois,
- privation de visites de sa famille, et notamment de son jeune enfant,
- refus pendant une longue période de lui permettre de communiquer par 
  téléphone avec ses proches

Suite à une plainte qu'elle qu'elle a pu finalement déposer, il est maintenant 
établi qu'elle a été gravement torturée à plusieurs reprises.

Nous demandons aux dirigeants de l'Etat iranien :

- l'annulation de la condamnation à mort de Sharifeeh Mohammadi,
- sa libération immédiate et l'arrêt des poursuites à son encontre,
- l'arrêt de l'usage de la torture,
- l'abolition de la peine de mort,
- la libération de l'ensemble des déten

Paris, le 12 juillet 2024

- Confédération française démocratique du travail (CFDT)
- Confédération générale du travail (CGT)
- Fédération syndicale unitaire (FSU)
- Union syndicale Solidaires
- Union nationale des syndicats autonomes (UNSA)


Videos
==========

Zohre
---------

- https://youtu.be/DVJeAC7m3N4

.. youtube:: DVJeAC7m3N4


Zoya
-------

- https://youtu.be/0w6-vZLCzcU

.. youtube:: 0w6-vZLCzcU


Cisem
----------

- https://youtu.be/ESQIzDNsV3Q

.. youtube:: ESQIzDNsV3Q

Mazdak
---------

- https://youtu.be/oX6PjOH28NY

.. youtube:: oX6PjOH28NY


Amnesty international
----------------------------

- https://youtu.be/iKSBJaNFkxs

.. youtube:: iKSBJaNFkxs

Zoya
---------

- https://youtu.be/QGe6_BgKZE0

.. youtube:: QGe6_BgKZE0

Sur mes réseaux sociaux
=======================

- https://kurdistan-au-feminin.fr/2024/07/10/iran-mobilisation-dans-la-prison-devin-pour-sharifeh-mohammadi/
