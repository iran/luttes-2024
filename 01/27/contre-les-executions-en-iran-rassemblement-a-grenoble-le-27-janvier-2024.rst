
.. index::
   pair: Rassemblement ; Contre les executions en iran rassemblement a grenoble le samedi 27 janvier 2024 (2024-01-27)


.. _iranluttes_2024_01_27:

=============================================================================================================================================================================================
♀️✊ ⚖️ **Femme, Vie, Liberté** 2024-01-27 **Contre les executions en Iran rassemblement à grenoble le samedi 27 janvier 2024 de 15h30 à 17h00 place Félix Poulat** |MohammadGhobalu|
=============================================================================================================================================================================================

- :ref:`iran_luttes:mahsa_jina_amini`
- :ref:`iran_luttes:slogans`


En hommage à Mohammad Ghobadlou |MohammadGhobalu|
=======================================================

- :ref:`iran_luttes:mohammad_ghobadlou`

Contre les executions en Iran rassemblement à grenoble le samedi 27 janvier 2024 de 15h30 à 17h00 place Félix Poulat
=========================================================================================================================

.. figure:: images/affiche_2024_01_27.webp

Rassemblement Contre les executions en Iran, annulation des condamnations
à mort, liberation des prisonniers politiques et expulsion de l'ambassadeur d'Iran.

::

    Jina Amini est morte le vendredi 16 septembre 2022
    ce samedi 27 janvier 2024 cela fait donc 1 an 4 mois 1 semaine 4 jours
    => 498e jour = on est dans la 72e semaine de la révolution




- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <iran_luttes:mahsa_jina_amini>` |JinaAmini|

Autres événements à Grenoble
==============================

- :ref:`iranluttes_2024_01_23`

Autres liens
==============

- https://travailleur-alpin.fr/2024/01/29/grenoble-la-repression-ne-faiblit-pas-en-iran/

