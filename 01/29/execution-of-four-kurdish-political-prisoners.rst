.. index::
   pair: Execution ; Execution of Four Kurdish Political Prisoners (2024-01-29)

.. _execution_2024_01_19:

====================================================================================================================================
2024-01-29 **Execution of Four Kurdish Political Prisoners #VafaAzarbar #MohsenMazloum #HajirFaramarzi #PejmanFatehi** |IranHr|
====================================================================================================================================

- https://iranhr.net/en/articles/6540/
- https://anfenglish.com/human-rights/iran-executes-4-kurdish-prisoners-71511
- https://kurdistan-au-feminin.fr/2024/01/29/iran-le-regime-enterrera-dans-un-lieu-secret-les-4-prisonniers-kurdes-executes-aujourdhui/


#VafaAzarbar #MohsenMazloum #HajirFaramarzi #PejmanFatehi

.. figure:: images/execution.webp

|IranHr| Execution of Four Kurdish Political Prisoners
============================================================

Iran Human Rights (IHRNGO); January 29, 2024: This morning, the Islamic
Republic executed four Kurdish political prisoners, Mohsen Mazloum, Pejman
Fatehi, Vafa Azarbar, and Hazhir Faramarzi, in Ghezel Hesar prison in Karaj,
**despite repeated domestic and international requests to stop the execution.**

**Iran Human Rights strongly condemns the execution of these four individuals
and calls on members of the international community to end their silence on
the wave of executions in Iran**.

The organization also urges Nada Al-Nashif, the UN Deputy High Commissioner
for Human Rights, to cancel her upcoming visit to Iran and explicitly
state that the reason is to protest these executions.

Mahmood Amiry-Moghaddam, director of the Iran Human Rights, said: "The execution
of these four prisoners was based on confessions under torture and without a
fair trial, and is considered extrajudicial killing. Khamenei and the corrupt
judiciary of the Islamic Republic must be held accountable for these murders."

He added: "The international community must show a practical response to the
Islamic Republic's rampant and daily executions. The least reaction of the UN
High Commissioner for Human Rights to the executions should be the cancellation
of Nada Al-Nashif's deputy commissioner's trip to Iran."


These four prisoners were convicted of "enmity against God and espionage for Israel."
---------------------------------------------------------------------------------------

According to the Islamic Republic news website,Tasnim, on the morning of Monday,
January 29, the execution of four Kurdish political prisoners was carried out
in Ghezel Hesar prison in Karaj. These four prisoners, identified as Mohsen
Mazloum, Pejman Fatehi, Vafa Azarbar, and Hezir Faramarzi,**were convicted of
"enmity against God and espionage for Israel."**

**These four Kurdish political prisoners were arrested in Urmia on June 22,
2021**, and were sentenced to death by Branch 26 of the Tehran Revolutionary
Court.

They were denied the right to meet or make phone calls with their families
and were held in an undisclosed location.

A few months after their arrest, media outlets close to security institutions
published videos of their forced confessions.

During their entire detention, these four prisoners were not allowed to meet
with their families or appointed lawyers.

**The only visit they were granted was a pre-execution meeting the day before.**

Iran Human Rights and many human rights defenders and organizations had called
for the stop and cancellation of the execution sentences of these four political
prisoners, citing the most basic principles of fair trial were not observed.


Other articles
==================

- https://anfenglish.com/human-rights/iran-executes-4-kurdish-prisoners-71511
- https://kurdistan-au-feminin.fr/2024/01/29/iran-le-regime-enterrera-dans-un-lieu-secret-les-4-prisonniers-kurdes-executes-aujourdhui/
- https://iran-hrm.com/2024/01/30/kurdish-political-prisoners-executed/
- https://anfenglish.com/human-rights/iran-does-not-hand-over-the-bodies-of-4-executed-kurds-to-their-families-71536
