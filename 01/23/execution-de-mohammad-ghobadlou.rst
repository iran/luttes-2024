.. index::
   pair: Pendaison; Mohammad Ghobadlou (2024-01-23)

.. _execution_mohammad_ghobadlou:

=======================================================================
2024-01-23 **Pendaison de Mohammad Ghobadlou** |MohammadGhobalu|
=======================================================================

- :ref:`iran_luttes:mohammad_ghobadlou`
- https://fr.wikipedia.org/wiki/Mohammad_Ghobadlou
- http://iranhr.net/en/articles/6521/

.. figure:: images/mohammad_ghobalu.webp
