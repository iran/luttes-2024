
.. index::
   pair: Conférence ; retour sur le mouvement de révolte iranien et les conditions d'exercide de la profession d'avocat en Iran (2024-01-23)


.. _iranluttes_2024_01_23:

==================================================================================================================================================================================================
2024-01-23 **Conférence retour sur le mouvement de révolte iranien avec Dorna Javan et Zohre Baharmast le mardi 23 janvier 2024 de 18h à 19h30** |MohammadGhobalu|
==================================================================================================================================================================================================


.. figure:: images/affiche_femme_vie_liberte_2024_01_23.webp


.. figure:: images/maison_de_lavocat.webp

Agenda
=======

- https://openagenda.com/evenements-grenoble-luttes/events/conference-retour-sur-le-mouvement-de-revolte-iranien-et-les-conditions-dexercice-de-la-profession-davocat-en-iran


En hommage à Mohammad Ghobadlou |MohammadGhobalu|
=======================================================

- :ref:`iran_luttes:mohammad_ghobadlou`

Description
============

Le mardi 23 janvier 2024 de 18h à 19h30 à la maison de l'avocat
45, rue Pierre Sémard
Grenoble

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.707995593547822%2C45.19115622985317%2C5.71153610944748%2C45.19275726600223&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.19196/5.70977">Afficher une carte plus grande</a></small>


Conférence retour sur le mouvement de révolte iranien et les conditions
d'exercice de la profession d'avocat en Iran avec:


**Dorna Javan**
-------------------

**Dorna Javan**, doctorante à Sciences Po Lyon


.. figure:: images/dorna_javan.webp


Zohre Baharmast
------------------

**Zohre Baharmast** de la ligue des Droits de l'Homme |ldh_iran|


.. figure:: images/dorna_zohre_intervenante.webp


19h30 Exposition photo et cocktail, interlude musical
=======================================================

.. figure:: images/cocktail.webp

.. figure:: images/non_au_hijab_obligatoire.webp

.. figure:: images/panneaux_sur_iran.webp


Brochure avocats
====================

.. figure:: images/brochure_avocat_en_danger.webp
.. figure:: images/le_barreau_iranien_sous_controle.webp
.. figure:: images/portrait_davocats.webp
.. figure:: images/situation_precaire_des_avocats.webp
