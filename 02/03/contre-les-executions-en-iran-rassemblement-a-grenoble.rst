
.. index::
   pair: Rassemblement ; Contre les executions en iran rassemblement a grenoble (2024-02-03)


.. _iranluttes_2024_02_03:

=============================================================================================================================================================================================
✊ ⚖️ 2024-02-03 **Contre les executions en Iran rassemblement à grenoble le samedi 3 février 2024 de 15h30 à 17h00 place Félix Poulat**
=============================================================================================================================================================================================

#StopExecutionsInIran #Grenoble #Rassemblement2024-02-03
#Iran #IranRevolution #DeathToTheIslamicRepublic

Contre les executions en Iran rassemblement à grenoble le samedi 3 février 2024 de 15h30 à 17h00 place Félix Poulat
=========================================================================================================================

.. figure:: images/affiche_2024_02_03.webp

Rassemblement Contre les executions en Iran, annulation des condamnations
à mort, liberation des prisonniers politiques et expulsion de l'ambassadeur d'Iran.
