.. index::
   pair: Rassemblement à Félix Poulat samedi 14 septembre 2024 de 15h à 17h pour la commémoration et hommage aux victimes du soulèvement Femme, Vie, Liberté ; 2024_09_14


.. _iranluttes_2024_09_14:

================================================================================================================================================================================================
2024-09-14 ✊ ⚖️ 📣 **Rassemblement à Grenoble place Félix Poulat samedi 14 septembre 2024 de 15h à 17h pour la commémoration et hommage aux victimes du soulèvement Femme, Vie, Liberté**
================================================================================================================================================================================================


🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 ) 
🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté #FemmeVieLiberte (Français, 🇫🇷)
🇮🇹 Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪) 

#IranRevolution #IranProtests2024 #ZanZendegiÂzâdi #JinJiyanAzadî #WomanLifeFreedom #FemmeVieLiberte 

.. figure:: images/appel_rassemblement.webp

Rassemblement à Félix Poulat samedi 14 septembre 2024 de 15h à 17h pour  la commémoration et hommage aux victimes du soulèvement Femme, Vie, Liberté
===========================================================================================================================================================

Avec 

- Amntesty International
- LDH Grenoble Métropole
- LDH Iran
- Voix d'Iran
- Iran Solidarité
- AIAK (Association Iséroise des Amis des Kurdes)


Sur mastodon
==================

- https://kolektiva.social/@iranluttes/113109044214425203


Les 3 otages français 📣
==================================

#FreeCecileKohler #FreeJacquesParis @FreeCecile_  #FreeOlivierGrondeau

- :ref:`iran_luttes:otages_francais`

- :ref:`iran_luttes:cecile_kohler` |CecileKohler|
- :ref:`iran_luttes:jacques_paris` |JacquesParis|
- :ref:`iran_luttes:olivier_grondeau` |OlivierGrondeau| 
