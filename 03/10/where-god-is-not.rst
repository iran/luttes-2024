.. index::
   pair: Film; Là où Dieu n'est pas
   pair: Film; Where god is not
   ! Mehran Tamadon


.. _film_where_god_is_not:

=============================================================================
2024-03-10 **Là où Dieu n'est pas (where god is not)** de Mehran Tamadon
=============================================================================


Description du film
===========================

- https://fifdh.org/festival/programme/2024/film/where-god-is-not/

Taghi, Homa et Mazyar ont été arrêté·es et interrogé·es par le régime iranien.

Tous les trois témoignent avec leurs corps et avec leurs gestes de ce que
signifie résister et craquer.

Y a-t-il un espoir que le tortionnaire renoue un jour avec sa conscience ?

Là où Dieu n’est pas est un film de témoignages de celles et ceux qui ont
été meurtri·es par le régime iranien.

Il cherche à comprendre le fonctionnement du système totalitaire dans sa
dimension la plus répressive, à savoir l’enfermement et la torture, en
utilisant le huit clos et la reconstitution comme formes cinématographiques.


Iran, les images de la répression
======================================

- https://fifdh.org/festival/programme/2024/forum/iran-les-images-de-la-repression/

Les prisons iraniennes sont remplies d’opposant·es politiques, régulièrement
torturé·es, maltraité·es, exécuté·es.
Comment dénoncer le traitement des dissident·es par le biais des images ?
Comment raconter la résistance et la résilience des iranien·nes ?

Là où Dieu n’est pas met en scène trois ex-prisonnier·ères : Taghi, Homa
et Mazyar ont été arrêté·es et interrogé·es par le régime iranien.

Chacun·e témoigne avec son corps et ses gestes de ce que signifie résister
et craquer. Y a-t-il un espoir que le tortionnaire renoue un jour avec sa conscience ?

Un film fort qui permet d’ouvrir la discussion sur la situation politique
en Iran et sur la manière dont le régime pratique la répression auprès de
ses opposant·es, en donnant la parole à celles et ceux qui l’ont vécue ou dénoncée.


Discussion
===============

Sepideh Farsi
-----------------

Film director and screenwriter
En savoir plus

Taghi Rahmani
------------------

Activiste et protagoniste du film
En savoir plus

Mehran Tamadon
-------------------

(en vidéoconférence) Cinéaste, réalisateur de Là où Dieu n'est pas
En savoir plus
Modération

Serge Michel
------------------

Journaliste, rédacteur en chef de Heidi.news et directeur des publications Kometa
En savoir plus


