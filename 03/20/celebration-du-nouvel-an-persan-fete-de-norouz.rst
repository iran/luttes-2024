.. index::
   pair: norouz ; Célébration du nouvel an persan fête de norouz (2024-03-20)

.. _norouz_2024_03_20:

=============================================================
2024-03-20 Célébration du nouvel an persan fête de Norouz
=============================================================

- :ref:`culture_perse:norouz`

Annonce
===========

Le 20 mars 2024 À partir de 16H Place Félix Poulat Grenoble
Rassemblement Goûter et musique avec le groupe iranien « Telekidan »

A l'appel de : Amnesty International, LDH Iran, SolidIran


**Les invités du Haft-Sîn (1) : victimes des manifestations**

(1) : les 7 objets symboliques de Norouz


.. figure:: images/affiche.webp


Description de Norouz
=========================

- https://fr.wikipedia.org/wiki/Norouz

Norouz (en persan: نوروز nowruz) est la fête traditionnelle des peuples iraniens
qui célèbrent le nouvel an du calendrier persan (premier jour du printemps).
La fête est célébrée par certaines communautés le 21 mars et par d'autres
le jour de l'équinoxe vernal, dont la date varie entre le 20 et le 22 mars.

Norouz a des origines iraniennes et zoroastriennes ; cependant, depuis plus
de 3000 ans, cette fête est célébrée par diverses communautés en Asie de l'Ouest,
Asie centrale, Caucase, bassin de la mer Noire, Balkans et Asie du sud.

C'est une fête culturelle et religieuse (voir Zoroastrianisme et Baha'i)

En français, Norouz est également appelé Nouvel An iranien ou Nouvel An persan5,6.

Le Norouz est inscrit à l'inventaire du patrimoine culturel immatériel en France
en 2019
