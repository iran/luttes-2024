.. index::
   pair: Spectacle ; Les poupées persanes, Atelier Théâtre Actuel à Crolles

.. _poupees_persanes_2023_03_22:

====================================================================================
Vendredi 22 mars 2024 **Les poupées persanes, Atelier Théâtre Actuel** à Crolles
====================================================================================

- https://espacepauljargot.crolles.fr/info-evenement/les-poupees-persanes-atelier-theatre-actuel/


.. figure:: images/les_poupees_persanes.png
   :align: center

   https://espacepauljargot.crolles.fr/info-evenement/les-poupees-persanes-atelier-theatre-actuel/

Espace Paul Jargot 191 Rue François Mitterrand, 38920 Crolles
=======================================================================

- https://espacepauljargot.crolles.fr/info-evenement/les-poupees-persanes-atelier-theatre-actuel/


::

    Les poupées persanes, Atelier Théâtre Actuel,
    Espace Paul Jargot 191 Rue François Mitterrand, 38920 Crolles
    22 mars 2024 / 19h30 de 7€ à 16€


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.882451832294464%2C45.27752130784551%2C5.885992348194123%2C45.27905574284647&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small>
   <a href="https://www.openstreetmap.org/#map=19/45.27829/5.88422">Afficher une carte plus grande</a></small>

Description
==============

Yeki bood, yeki nabood, gheir az khodâ, itch kas nabood…

C’est l’histoire de quatre universitaires dans l’Iran des années 1970,
de la chute du Shah à l’arrivée au pouvoir du régime islamique.

C’est en France l’histoire de deux sœurs venues célébrer en famille le
passage à l’an 2000 à Avoriaz, et qui ont hérité de bribes d’une histoire
venue d’ailleurs.

C’est aussi l’histoire d’amour de Bijan et Manijeh, couple mythique des
légendes perses ; l’histoire enfin d’une jeunesse pleine d’espoir,
d’une lutte avortée, d’un peuple sacrifié, de secrets qui s’entortillent,
de la transmission dont on ne sait que faire et de l’amour qui ne sait
plus où aller.

C’est l’histoire de toutes ces histoires en poupées gigognes, et à vrai
dire celle de toutes les révolutions…

2 récompenses aux Molières 2023 !

Entre rire et larmes, on embrasse ces poupées qui se jouent de nous avec
un plaisir immense. Sous des airs badins de comédie, la pièce porte un
propos bien plus profond, et extrêmement touchant.

Détendu par l’humour, on se fait cueillir. Touché en plein cœur. Le Parisien

Autour du spectacle
==========================

- https://www.uicg.fr/

Bord plateau à l’issue de la représentation

Jeudi 21 mars 2024 20h – médiathèque Gilbert Dalet Conférence en partenariat
avec `l’UICG <https://www.uicg.fr/>`_, par Yves Santamaria, qui apportera son regard d’historien
sur l’Iran des années 70. Entrée libre

Temps fort en médiathèque sur le conte iranien et du Moyen-Orient.


Distribution
==============

- Texte d’Aïda Asgharzadeh
- Mise en scène Régis Vallée
- Assistante à la mise en scène Mélissa Meyer
- Avec Aïda Asgharzadeh, Ariane Mourier, Toufan Manoutcheri, Sylvain Mossot,
  Kamel isker, Aziz Kabouche, Marion Rebmann
- Musique Manuel Peskine
- Vidéos Fred Heusse
- Création lumière & régie générale Aleth Depeyre
- Perruques Julie Poulain
- Costumes Marion Rebmann, assistée de Marie Dumas de la Roque
- Décors Philippe Jasko, Régis Vallée


Mentions
============

- https://www.atelier-theatre-actuel.com/
- https://www.atelier-theatre-actuel.com/qui-sommes-nous/presentation/

Une production ACME, `Atelier Théâtre Actuel <https://www.atelier-theatre-actuel.com/>`_
et Les Béliers Parisiens

