.. index::
   pair: Concert ; Nima Sarkechik
   pair: Concert ; Lomir Zingen
   ! Concert de la Fraternité (2024-03-03)

.. _fraternite_20245_03_03:

===========================================================================================================================================
**Concert de la fraternité** avec Lomir Zingen, Humaine Cie et Nima Sarkechik le dimanche 3 mars 2024 à 14h, salle Olivier Messiaen
===========================================================================================================================================

.. figure:: images/concert_fraternite.webp
   :width: 500


Concert de la fraternité
============================

Concert de la fraternité avec:

- :ref:`Lomir Zingen <lomir_zingen>`, chants #Yiddish
- Humaine Cie, chanson française
- :ref:`Nima Sarkechik <nima_sarkechik>`, pianiste

Lieu
    Salle Olivier Messiaen, 1 rue du Vieux Temple 38000 Grenoble


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.73139786720276%2C45.19256257333884%2C5.734938383102418%2C45.19409930410641&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.19333/5.73317">Afficher une carte plus grande</a></small>


Réservation
    06 80 70 88 25


.. _lomir_zingen:

**Lomir Zingen**
==================

- https://www.facebook.com/lomirzingenStrasbourg/

Choeur amateur strasbourgeois spécialisé dans la chanson yiddish, mais
pas que, et la transmission de la langue et la culture yiddish.


.. _nima_sarkechik:

**Nima Sarkechik**
=====================

- http://www.nimasarkechik.com/page/biographie
- http://www.nimasarkechik.com/

Ses concerts remarqués à La National Gallery of Arts et au Kennedy Center de
Washington couronnent une carrière internationale florissante. De Rome à New
York, en passant par Bogota et Tel Aviv, le jeune pianiste franco-iranien est
acclamé dans les salles les plus prestigieuses du monde entier : National Museum
de Londres, Lincoln Center de New York, Printemps des Arts de Monte-Carlo, Palais
des Beaux-Arts de Bruxelles, Usina del Arte de Buenos Aires, Opéra d'Odessa,
Festival de Saint-Ursanne, Palais de l’Athénée à Genève, Jerusalem Music
Center, Musicales de Compesières, Festival de Gstaad, Festival de Neümunster,
Villa Medicis de Rome, Festival des Açores, Festival Chopin de Bratislava,
Festival Pianistico de Moldavie, Kazakhkontsert d'Almaty, régulièrement au
Maghreb et au Proche-Orient (Maroc, Turquie, Algérie, Jordanie, Egypte).

On l’entend en France à la Folle Journée de Nantes, au Festival de piano
de la Roque d’Anthéron, au Théâtre des Champs-Elysées, à la Cité de
la Musique, à la Salle Gaveau, au Festival de Radio-France à Montpellier,
à l’Auditorium du Musée d’Orsay, au Palais des congrès de Strasbourg,
au Festival des Nuits romantiques du Lac du Bourget, à l'Opéra de Limoges...

Lien Arash Sarkechik
-------------------------

- :ref:`iran_luttes_2023:concert_live_2023_09_30`
