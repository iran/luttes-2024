.. index::
   pair: Narges Mohammadi ; Citoyenne d'honneur de la commune de La Buisse (2024-11-25)

.. _narges_2024_11_25:

=============================================================================================================================
2024-11-25 **Narges Mohammadi Militante iranienne des droits humains Citoyenne d'honneur de la commune de La Buisse**
=============================================================================================================================

- https://fr.wikipedia.org/wiki/La_Buisse
- https://fr.wikipedia.org/wiki/Narges_Mohammadi
- :ref:`iran_luttes:narges_mohammadi`


Origine
============

- https://www.ledauphine.com/societe/2024/11/10/narges-mohammadi-prix-nobel-de-la-paix-bientot-citoyenne-d-honneur-de-la-buisse

La Buisse, petite commune iséroise de 3 400 habitants située au pied du 
massif de la Chartreuse dans le Pays voironnais, a décidé de décerner 
le titre de citoyenne d’honneur à Narges Mohammadi, militante des droits 
humains, emprisonnée en Iran, et lauréate du prix Nobel de la paix en 2023. 

Une décision qui peut paraître étonnante de prime abord.
L’idée en revient à Jean-Marc Attali, conseiller municipal délégué à la 
culture et au patrimoine qui est aussi le responsable du groupe local
d'Amnesty International.


Invitation
===============

.. figure:: images/invitation_buisse.webp
   :width: 300

**Dominique Dessez, Maire de La Buisse, et Jean-Marc Attali, Conseiller municipal 
délégué**, vous invitent à participer à la  cérémonie organisée en l'honneur 
de `Narges Mohammadi <https://fr.wikipedia.org/wiki/Narges_Mohammadi>`_, 
lundi 25 novembre 2024, 18h à la salle socio-educative (socio-culturelle, NDLR).


Lieu : La salle socio-culturelle
--------------------------------------------------------------------------

- https://www.openstreetmap.org/#map=18/45.332375/5.614926

Accès à pied par la rue de Chantabot ou la rue de la Marelle.


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.615156292915345%2C45.332640548172606%2C5.624114871025086%2C45.335842147373775&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/45.334241/5.619636">Afficher une carte plus grande</a></small>
   


Vidéos des discours et chants
=================================

- https://youtu.be/HNEmi6YcO7Y

.. youtube:: HNEmi6YcO7Y   
   
Zoya et Patrick à la déclamation
======================================
   
- https://www.youtube.com/watch?v=gZ82OoAfcbs   
   
.. youtube:: gZ82OoAfcbs   
   
Narges Mohammadi
===========================

- https://fr.wikipedia.org/wiki/Narges_Mohammadi

.. figure:: images/narges.webp


`Narges Mohammadi <https://fr.wikipedia.org/wiki/Narges_Mohammadi>`_ Militante iranienne des droits humains, est  
incarcérée depuis de nombreuses années dans les prisons de son pays. 

Elle s'est vue décerner en décembre 2023 le **Prix Nobel  de la Paix** 
pour son engagement contre l'oppression des femmes en Iran, et pour la 
défense de la liberté et des droits humains. 

Le conseil municipal de La Buisse a décidé de lui conférer le titre de **Citoyenne d'honneur de la commune**
===================================================================================================================

Le conseil municipal de La Buisse a décidé de lui conférer le titre de 
**Citoyenne d'honneur de la commune**.


Photos
==========

.. figure:: images/20241125_180103_800.webp  
.. figure:: images/20241125_193210_800.webp  
.. figure:: images/20241125_194555_800.webp
.. figure:: images/20241125_185521_800.webp  
.. figure:: images/20241125_193218_800.webp  
.. figure:: images/20241125_194624_800.webp
.. figure:: images/20241125_190556_800.webp  
.. figure:: images/20241125_194526_800.webp  
.. figure:: images/20241125_194827_800.webp
.. figure:: images/20241125_193159_800.webp  
.. figure:: images/20241125_194540_800.webp


Autres liens 
=============

- :ref:`iran_exposition:iran_exposition`
