.. index::
   pair: Ahou Daryaei; Paris, place du Panthéon : première manif pour sauver Ahou Daryaei (2024-11-05)

.. _ahou_daryaei_2024_11_05:

=====================================================================================================
2024-11-05 **Paris, place du Panthéon : première manif pour sauver Ahou Daryaei** par aplutsoc
=====================================================================================================

- https://aplutsoc.org/2024/11/05/paris-place-du-pantheon-premiere-manif-pour-sauver-ahou-daryaei/


.. figure:: images/appel.webp

.. figure:: images/relai.webp

.. figure:: images/dessin_ahou_debout.webp


Comment s’appelle-t-elle ? Ahou Daryaei, 30 ans, mère de deux enfants, 
doctorante en français. 

Les nervis islamistes l’ont embarquée. 

Elle aurait balancé sa tenue imposée après avoir été embêtée pour un 
voile mal mis. Elle est sans doute en danger de mort, de viol, etc., de 
la part des défenseuses et défenseurs de la vertu et la religion, ces pervers.

Son geste s’inscrit dans la lignée des grandes narodniks qui défiaient 
les tsars au XIX° siècle, des combattantes et des combattants de la liberté 
et de l’émancipation. 

C’est un geste qui, à la différence de tous les missiles de Netanyahou, 
porte, lui, un coup direct dans le vrai cœur battant de l’ordre social-patriarcal 
et terroriste-religieux du berceau mondial de la contre-révolution du 
voile islamique, ce drapeau mondial de l’oppression. 

Ce mensonge tombera, d’abord dans son berceau et par là dans le monde entier.

Il faut sauver Ahou Daryaei.


