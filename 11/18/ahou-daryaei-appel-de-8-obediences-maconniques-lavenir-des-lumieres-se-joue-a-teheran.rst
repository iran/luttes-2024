.. index::
   pair: Ahou Daryaei; Paris, place du Panthéon : première manif pour sauver Ahou Daryaei (2024-11-05)

.. _ahou_daryaei_2024_11_18:

============================================================================================================================
2024-11-18 **Ahou Daryaei : appel de 8 obédiences maçonniques L’avenir des Lumières se joue à Téhéran** par aplutsoc
============================================================================================================================

- https://aplutsoc.org/2024/11/05/paris-place-du-pantheon-premiere-manif-pour-sauver-ahou-daryaei/

Ahou Daryaei : appel de 8 obédiences maçonniques

Appel à Monsieur le Président de la République,
Appel à tous les élus, toutes les élues,
Appel à toutes les forces de progrès.

Vous ne pouvez rester silencieux devant ce nouvel acte de résistance 
d’une étudiante iranienne Ahou Daryaei qui en se dévêtant publiquement 
a transformé son corps en manifeste vivant contre un régime de coercition
absolue qui étouffe la liberté des femmes. 

Son acte, aussi immensément courageux que symbolique, porte la voix de 
millions de femmes dans le monde, réduites au silence par le fanatisme 
religieux. 

Il porte la voix des femmes qui se battent en Iran contre un pouvoir 
théocratique et obscurantiste.

Cet acte courageux nous rappelle une vérité fondamentale : la liberté 
n'est pas un privilège mais un droit inaliénable, non négociable.

Le combat des Iraniennes transcende les frontières et nous concerne tous 
et toutes. 
Il nous rappelle que la dignité humaine est indivisible. 

Quand une femme est privée de ses droits fondamentaux, c'est l'humanité
entière qui en est privée.

Nous ne pouvons pas, à longueur de temps, déclarer notre solidarité avec 
toutes les victimes et, aujourd’hui, nous taire. 

Il y a des silences complices que nous ne pouvons accepter. 

La lutte de ces femmes doit être la nôtre, nous devons amplifier leurs voix, 
relayer leur message, et maintenir la pression sur ceux qui violent les
droits humains, à Téhéran et partout. 

Ailleurs comme en Afghanistan aussi.

Le chemin vers la liberté est long, mais chaque acte de résistance, 
chaque voix qui s'élève rapproche un peu plus du jour où les femmes, 
dans le monde pourront enfin s’éduquer, travailler, s'habiller et s'exprimer
librement, en un mot vivre

Exigeons la libération d’Ahou Daryaei, tout de suite et maintenant. 

Condamnons les agissements du régime des Mollahs !

À toutes les femmes d'Iran qui résistent pacifiquement, qui persistent 
malgré la répression, nous disons : « votre courage nous inspire, votre 
combat est le nôtre, votre idéal est la part la plus lumineuse de notre commune
humanité ». 

L’avenir des Lumières se joue à Téhéran.

Paris, le 18 novembre 2024

Nicolas PENIN, Grand Maître du Grand Orient de France
Sylvain ZEGHNI, Grand Maître de la Fédération Française de l'Ordre Maçonnique Mixte International Le Droit Humain
Liliane MIRVILLE, Grande Maîtresse de la Grande Loge Féminine de France
Bernard DEKOKER-SUAREZ, Grand Maître de la Grande Loge Mixte Universelle
Félix NATALI, Grand Maître de la Grande Loge Mixte de France
Philippe CANGEMI, Grand Maître de la Grande Loge Traditionnelle et Symbolique Opéra
Paulina CERVANTES, Grand Maître adjoint Gran Oriente Latinoamericano
Catherine MINOUFLET, Grand Maître de la Grande Loge Mixte de Memphis-Misraïm
