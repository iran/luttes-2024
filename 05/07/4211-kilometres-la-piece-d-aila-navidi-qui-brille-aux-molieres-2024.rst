.. index::
   pair: Léa Degay; 4211 kilomètres, la pièce qui brille aux Molières 2024
   pair: Aila Navidi ; 4211 kilomètres, la pièce qui brille aux Molières 2024 (2024-05-07)


.. _degay_2024_05_07:
.. _navidi_2024_05_07:

=======================================================================================================
2024-05-07 **4211 kilomètres, la pièce d'Aila Navidi qui brille aux Molières 2024**
=======================================================================================================


#AilaNavidi #4211Kilomètres
#FreeToomaj #FreeSaman #ToomajSalehi #WomenLifeFreedom #MahsaAmini #IranRevolution
#NoToExecutionsInIran #NoToDeathPenalty #IrgcTerrorists #NoToIslamicRepublic
#SaveToomaj


.. figure:: images/aila_navidi.jpg

   Aila Navidi


.. figure:: images/akrami.jpg

   https://x.com/Baboobabounette/status/1787616171396608511
   Bravo à ma cousine de ♥️,  le sang de la veine 🩸, Aïla Navidi pour le
   Molière reçu ce soir pour sa magnifique pièce 4211km.

   Œuvre qui parle du combat des Iraniennes et Iraniens contre la dictature
   et leur exil.

   Merci d’avoir parlé de Toomaj et de Femme vie liberté !


4211 kilomètres, la pièce qui brille aux Molières 2024
==========================================================

- https://lepetitjournal.com/expat-mag/culture/4211-kilometres-piece-molieres-2024-384724

4211 kilomètres c’est la distance qui sépare Téhéran de Paris, celle parcourue
par Mina et Fereydoun, les parents de Yalda, venus d’Iran pour se réfugier
en France à la fin des années 70.

Mais surtout, **c’est le nom de la pièce de théâtre écrite et mise en scène
par Aïla Navidi**.

.. youtube:: OrZEvJHHrRY


Lundi 6 mai 2024, aux Molières, 4211 kilomètres brille avec deux récompenses.

4211 kilomètres raconte l’histoire d’un héritage culturel et familial que
Mina, Fereydoun et leur fille, Yalda, aiment et détestent.

Les trois personnages sont face à une quête identitaire, ne plus être entièrement
iranien mais pas totalement français.

A travers cette pièce, Aïla Navidi raconte sa propre histoire : l’exil de
ses parents de la capitale iranienne vers la capitale française lorsqu’elle
était dans le ventre de sa maman.

La pièce a été récompensée par deux Molières : meilleur spectacle du côté du
théâtre privé et révélation féminine pour Olivia Pavlou-Graham, qui joue Yalda.

4211 kilomètres résonne de manière particulière alors que le peuple iranien
se révolte encore et toujours, suite à l’assassinat de Masha Amini, en septembre 2022.

4211 kilomètres se veut éclairante **sur la barbarie du régime islamique et un
témoin du combat que les Iraniens mènent depuis plus de 40 ans pour ceux et
celles qui ne sont plus, la population toujours sur place et celle qui s’est exilée**.


Ses deux statuettes, Aïla Navidi les dédie à “tous les exilés, tous les déracinés
et à tous ceux qui se battent pour la liberté”.

Elle invite également les dirigeants à exiger la libération de :ref:`Toomaj Salehi <iran_luttes:toomaj>`,
un rappeur iranien de 33 ans |toomaj|.

Depuis le 24 avril 2024, il est condamné à mort pour “corruption sur terre”.


.. figure:: images/akrami_navidi.jpg


ArticlesVidéos  2024
=======================

Aïla Navidi : "La naissance de mon enfant m’a fait m'interroger sur ce qui allait rester de ce récit
---------------------------------------------------------------------------------------------------------

- https://www.youtube.com/watch?v=p12uSe8XxiY


Aussi aux Molières 2024 : **Molières 2024: Sophia Aram dénonce le "silence" du monde de la culture envers les victimes du 7 octobre 2023**
==================================================================================================================================================================

- :ref:`raar_2024:aram_2024_05_07`
