.. index::
   pair: CGT ; Il faut sauver Toomaj Salehi et Saman Yasin ! (2024-05-07)

.. _cgt_2024_05_07:

===========================================================================================================================================================
2024-05-07 **Libération sans condition de Toomaj Salehi et Saman Yasin liberté totale pour les artistes en iran ! non à la peine de mort !"** |toomaj|
===========================================================================================================================================================

- :ref:`iran_luttes:toomaj`

.. figure:: images/free_toomaj_salehi_and_saman_yasin.webp


#FreeToomaj #FreeSaman #ToomajSalehi #SamanYasin #WomenLifeFreedom #MahsaAmini #IranRevolution
#NoToExecutionsInIran #NoToDeathPenalty #IrgcTerrorists #NoToIslamicRepublic
#SaveToomaj #SaveSamanYasin

Contact
========

- cgtculture38@gmail


**Libération sans condition de Toomaj Salehi et Saman Yasin liberté totale pour les artistes en iran ! non à la peine de mort !**
===================================================================================================================================

Connu pour ses chansons critiques du pouvoir iranien, dénonçant les injustices
sociales et les inégalités institutionnelles entres les femmes et les hommes
dans son pays, le rappeur Toomaj Salehi, accusé d' "incitation à la sédition,
rassemblement, conspiration, propagande contre le système et appel aux émeutes",
vient d’être condamné à mort le 24 avril 2024 par la première section du
tribunal révolutionnaire d'Ispahan "pour corruption sur Terre".

Arrêté en octobre 2022, libéré sous caution, il a été arrêté à nouveau pour avoir
dénoncé dans une vidéo les tortures dont il avait été victime.

Ce rappeur populaire iranien avait soutenu dans ses chansons et sur les réseaux
sociaux le puissant mouvement de contestation "Femme, Vie, Liberté" déclenché
après la mort de Jina Mahsa Amini en septembre 2022.

Depuis le déclenchement du soulèvement "Femmes, Vie, Liberté" en septembre 2022,
469 personnes ont été tuées par les forces de sécurités iraniennes et 22000 personnes
ont été arrêtées.

L'Iran pratique la peine capitale à grande échelle.
Amnesty International a recensé 853 exécutions en 2023.

Marjane Satrapi, la célèbre autrice de "Persepolis", qui milite pour la libération
de l’artiste parle de Toomaj Salehi en ces termes :

    "Il est le seul rappeur iranien qui parle des classes ouvrières et des
    défavorisés.
    Ses textes sont politiques et très beaux.
    C’est un poète dont les mots n’appellent pas à la violence mais ils
    appellent au réveil.

    Il est devenu un symbole pour tout un pays.

    La libération de Toomaj voudrait dire la libération de la parole en Iran.".

Son collègue rappeur Saman Yasin (Seydi) également ar.ste engagé, incarcéré
depuis le 2 octobre a été accusé "d’avoir mené une guerre à Dieu",
condamné à 5 ans de prison après avoir encouru la peine de mort


Appel de La SELF38 CGT Spectacle Culture
=============================================

La SELF38 CGT Spectacle Culture appelle:

- à la libération sans condition de Toomaj Salehi et Saman Yasin
  ainsi que de tous les prisonniers politiques en Iran,
- à l'abolition de la peine de mort et des tortures.
- à la mobilisation de tous les artistes pour exiger la libération de Toomaj Salehi.
- Au soutien de toutes les organisations qui défendent la liberté d’expression
  des artistes et des citoyen·nes en Iran
- à l’interpellation urgente des autorités Iraniennes

.. figure:: images/toomaj.png


Organisations
====================

Les organisations et personnalités de la culture co-signataires de ce communiqué,
appellent à un rassemblement mardi 7 mai 2024 à 18H devant la MC2 à Grenoble

Premiers signataires du secteur culturel
---------------------------------------------

- SELF38 CGT Spectacle Culture, SYNAVI Auvergne rhône alpes;
- Myla Al Messa, chorégraphe;
- Anagramme;
- Cie Artiflette;
- Fanette Arnaud bibliothécaire à la retraite;
- Ismaël Bahri auteur, compositeur interprète (rap) et technicien (scaff);
- Pascale Barouline alias La Princesse Barouline, artiste du spectacle vivant;
- Yves Béal, poète écrivain;
- Véronique Boulard, comédienne;
- Tomas Bozzato, réalisateur;
- Elvire Capezzali, comédienne;
- Enzo Corato, technicien plateau;
- Camille Cottalorda, costumière scénographe road scaff rigger;
- Benjamin Croizy, musicien et régisseur spectacle vivant;
- Denis Cugnod, réalisateur;
- Jean-Luc Donnet, technicien vidéo;
- Matthieu Dupuis technicien du spectacle;
- Cie Kikeï;
- Stéphène Jourdain, Directeur du Centre des Arts du Récit;
- Amazigh Kateb, chanteur;
- Benoît Chabert d’Hieres, ingénieur du son;
- Elisabeth Calandry, conteuse; Céline Doubrovik, danseuse;
- Olivia Ferrero, réalisatrice;
- Olaf Fabiani, comédien, circassien, metteur en scène;
- Stéphanie Giet, technicienne du spectacle;
- Patrice Giraud alias CRÈVECŒUR chanteur de rues et joueur d'orgue de Barbarie;
- Josefa Gallardo directrice La Rampe La ponatière;
- Gaspard Gonthier technicien intermittent du spectacle;
- Cécile Gauthier, directrice du pôle relations au public Hexagone Scène nationale Meylan;
- Jeanne Guillon, comédienne;
- Hervé Haggaï, comédien, metteur en scène;
- Fanny Hermant, autrice metteuse en scène;
- Raphaëlle Jay, comédienne;
- Lise Landrin Astrid Ligier présidente du collectif A Ta Sauce;
- Lisa Lehoux, comédienne et chanteuse; Géraldine Michel régisseuse lumière;
- Elodie Morard;
- Camille Pasquier Comédienne;
- Emeline Nguyen, chorégraphe Cie La GueHeuse;
- Luc Quinton, plasticien colleur d’histoires;
- Georges Pin, directeur du Conservatoire Jean Wiener;
- Véronique Pédréro, Artiste conteuse-autrice-céramiste;
- Henri Errico, président Association Les CE tissent la toile;
- Michel Szempruch, réalisateur;
- Edouard Schoene, militant associatif, ancien adjoint à la culture;
- Gilbert Dombrowsky, comédien; Association La nuit du renard;
- Dora Caicedo, compositrice musicienne;
- jean-Christophe Houde, réalisateur;
- Cecile Lacroix, conteuse; Jean-Claude Lamarche, artiste peintre, responsable
  des pages culture du Travailleur alpin;
- Tom Guichard, danseur, interprète, chorégraphe; Raphaëlle Jay, comédienne;
- Marc Lambelin, technicien;
- Suzelle Maitre;
- Arnaud Meunier, Directeur MC2;
- Marcel Morize, musicien;
- Anouk Nier-Nantes, artiste;
- Annelise Pizot, danseuse chorégraphe;
- Colette Priou chorégraphe;
- Pascale Puig, directrice Mon Ciné;
- Juliette Rizoud - responsable artistique et administrative La Bande à Mandrin;
- Christophe Sacchekni, musicien;
- Gallia Semory, enseignante-artiste;
- Régis Soucheyre, chanteur;
- Violaine Soulier, musicienne;
- Filippo Tarricone, technicien;
- Simon Tarricone, serveur;
- Ienisseï Teicher, danseuse;
- Myriam Vienot, comédienne;
- Coralie Weiderdong, comédienne, clown;


Premiers signataires des organisations en soutien
-----------------------------------------------------

- Ligue des Droits de l’Homme Grenoble Metropole;
- LDH Iran;
- Amnesty InternaFonal Grenoble
- CISEM;
- Iran Solidarité
- Association Iséroise des amis des kurdes (AIAK)
- Le collectif "17 octobre 1961 Isère"
- ATTAC38;
- Mouvement pour la Paix Isère;
- Alternatiba/ANV-COP 21 Grenoble;
- Ensemble Isère;
- La France Issoumise 38;
- NPA l’anticapitaliste 38;
- PCF 38;
- UL CGT Grenoble;
- Les Ecologistes EELV Isère;
- Solidaires 38;
- UD CGT Isère;
- FSU Isère;
- SAF 38;
- Sud Lutte de classe - Education;
- Association ACIP-ASADO (Association pour la Coopération Inter-Peuples Action
  de Solidarité avec les peuples d’Amérique latine et pour la Diffusion des cultures d’Origine);
- Cercle Laïque;
- UCL 38;

Premiers élu.es en soutien
===========================

- Simon Billouet, conseiller départemental de l’Isère;
- Guillaume Gontard, sénateur de l’isère;
- Jacqueline Madrennes, adjointe à la culture d'Echirolles

Tract
=======

:download:`Télécharger le tract au format PDF <pdfs/Appel_liberation_de_TOOMAJ_et_SAMAN_V5_2024_05_23.pdf>`

Quelques photos
====================

.. figure:: images/20240507_180311_800_mise_en_place.webp
.. figure:: images/20240507_180352_800_liberez_toomaj.webp
.. figure:: images/20240507_180641_800_liberez_toomaj_2.webp
.. figure:: images/20240507_180659_800_non_aux_executions_en_iran.webp
.. figure:: images/20240507_180841_800_zoya_non_non_non.webp
.. figure:: images/20240507_181126_800_vue_mc2.webp
.. figure:: images/20240507_181132_800_mc2_droit.webp
.. figure:: images/20240507_181202_800_mc2_droit_zoya.webp

Quelques vidéos
==================

Discours du camarade de La SELF38 CGT Spectacle Culture pour la libération de Toomaj Salehi
-----------------------------------------------------------------------------------------------

.. youtube:: kQ6RuOrFUAA

Discours du camarade du NPA pour la libération de Toomaj Salehi
-----------------------------------------------------------------------------------------------

.. youtube:: AKRVuXKK0DA

Discours de Zoya sur l'Iran et la libération de Toomaj Salehi
-----------------------------------------------------------------------------------------------


Référence du :ref:`livre La face cachée des mollahs par Emmanuel RAZAVI <iran_luttes:mollahs_2024_01>`

.. youtube:: D91_rRgARrk


Discours de la camarade d'Amnesty International sur la répression en Iran
-----------------------------------------------------------------------------------------------

.. youtube:: L-mPNMR7ziA

