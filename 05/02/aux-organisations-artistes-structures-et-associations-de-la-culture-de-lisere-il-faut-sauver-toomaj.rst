.. index::
   pair: CGT ; Aux organisations, artistes, structures et associations de la culture de l’Isère IL FAUT SAUVER TOOMAJ ! (2024-05-02)

.. _cgt_2024_05_02:

==========================================================================================================================
2024-05-02 **Aux organisations, artistes, structures et associations de la culture de l’Isère IL FAUT SAUVER TOOMAJ !"**
==========================================================================================================================

.. figure:: images/toomaj.png


#FreeToomaj #FreeSaman #ToomajSalehi #SamanYasin #WomenLifeFreedom #MahsaAmini #IranRevolution
#NoToExecutionsInIran #NoToDeathPenalty #IrgcTerrorists #NoToIslamicRepublic
#SaveToomaj #SaveSamanYasin

Appel
=======

La Section Locale Fédérale CGT Spectacle Culture de l’Isère appelle à se
mobiliser pour sauver Toomaj Salehi rappeur iranien récemment condamné à mort
par le tribunal d'Ispahan.

**Nous devons tout faire pour exiger sa libération**.

Nous vous proposons dans un premier temps de co-signer cet appel ci-joint au
plus tard dimanche, rassembler un maximum de signatures, dans l’idéal demain soir
ou samedi matin pour envoyer cet appel dans le réseau artistique et  culturel
grenoblois afin de mobiliser pour le rassemblement organisé le mardi 7 mai 2024
à 18H devant la MC2.

Nous y inviterons la presse et organiserons des prises de paroles des soutiens.

envoyer vos signatures à : cgtculture38@gmail.com

ou par texto à Michel SZEMPRUCH au 06 10 84 63 97

Ce texte sera envoyé également aux associations de défense des droits de l’Homme,
et plus largement aux organisations associatives, syndicales et politiques.

Un communiqué sera envoyé à la presse lundi 6 mai 2024 à 10H.


L’équipe de la SELF38 CGT SPECTACLE CULTURE


Tract
=======

:download:`Télécharger le tract au format PDF <pdfs/Appel_liberation_de_TOOMAJ_2024_05_02.pdf>`
