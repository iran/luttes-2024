.. index::
   ! Ebrahim Raïssi
   ! Hossein Amir-Abdollahian

.. _raissi_2025_05_19:
.. _iranluttes_2025_05_19:

============================================================================================================================================
2024-05-19 **Cinq jours de fête pour célébrer la mort du "boucher de Téhéran" Ebrahim Raïssi et Hossein Amir-Abdollahian** #Helikotlet 🚁
============================================================================================================================================

- https://x.com/LettresTeheran/status/1792442607236223009


.. figure:: images/cheveux_de_femmes_1.webp

.. figure:: images/cheveux_de_femmes_2.webp


.. figure:: images/ebrahim_raissi.webp
   :width: 500

   https://x.com/LettresTeheran/status/1792442607236223009

#Iran #Syrie #Raïssi #FemmeVieLiberté

#helikotlet

C'est confirmé, Ebrahim Raïssi, surnommé le boucher de Téhéran, est mort dans
un crash d'hélicoptère.

**Il n'aura donc jamais à répondre de son implication dans le massacre des
prisonniers politiques en 1988**.

Sa présidence a été marquée par la répression sanglante des manifestants, une
inflation galopante, une corruption généralisée et la mainmise total du CGRI
sur l'économie et l'État en Iran.

Son successeur, qui prendra le pouvoir après une mascarade électorale, ne
fera pas mieux car Khamenei et ses gardiens de la révolution sont les seuls maîtres du pays.

.. figure:: images/kotlet.webp

   Qui vit comme un boucher fini souvent en kotlet !

   - https://x.com/sdblepas/status/1792247815378272516
   - https://x.com/sdblepas/status/1792218495888834773




"Personne n’a été tué !". Nombreux se rappellent aujourd'hui de cette réponse de Hossein Amir-Abdollahian, l’ex-ministre des Affaires étrangères du régime
==============================================================================================================================================================

- https://x.com/LettresTeheran/status/1792472927457194032

« Personne n’a été tué ! ».

Nombreux se rappellent aujourd'hui de cette réponse de Hossein Amir-Abdollahian,
l’ex-ministre des Affaires étrangères du régime, à un journaliste après sa
question sur les manifestants tués.

Il a également péri dans le crash de l'hélicoptère 🚁.


.. figure:: images/hossein_amir_abdollahian.webp

   https://x.com/LettresTeheran/status/1792472927457194032


Liens
========

- :ref:`akrami_iran:baboush_5_2024_05_20`
