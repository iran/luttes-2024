.. index::
   ! Commémoration du premier anniversaire de l’exécution des trois jeunes innocents par la République islamique d’Iran "Majid Kazemi, Saeed Yaghoubi, Saleh Mirhashemi" (2024-05-18)

.. _iranluttes_2024_05_18:

===================================================================================================================================================================================================================================================================================================================
2024-05-18 ⚖️ 📣 à Grenoble, place Félix Poulat de 15 à 17h **Tous ensemble contre les exécutions en #Iran Commémoration du premier anniversaire de l’exécution des trois jeunes innocents par la République islamique d’Iran "Majid Kazemi, Saeed Yaghoubi, Saleh Mirhashemi"**
===================================================================================================================================================================================================================================================================================================================

- https://www.instagram.com/global_no_to_execution_in_iran/

#MajidKazemi #SaeedYaghoubi #SalehMirhashemi


Sources
=========

- https://www.instagram.com/p/C60cGhiNUrb/?utm_source=ig_web_copy_link
- https://www.instagram.com/p/C65Zn5YNwuJ/?utm_source=ig_web_copy_link


.. figure:: images/affiche.webp

.. figure:: images/affiche_poulat.webp


Commémoration du premier anniversaire de l’exécution des trois jeunes innocents par la République islamique d’Iran "Majid Kazemi, Saeed Yaghoubi, Saleh Mirhashemi"
=========================================================================================================================================================================================

📣 "Ne les laissez pas nous tuer !" |StopExecution|

Ce furent les derniers mots des jeunes hommes dans l’affaire de la "Maison d’Isfahan"
- Majid, Saeed et Saleh - griffonnés sur un morceau de papier froissé.

À l’aube, ils étaient partis, ne laissant derrière eux que le bourreau et sa potence.

La République islamique d’ #Iran continue de chercher des cous à pendre et
les vies précieuses de Toomaj, Abbas, Reza, Mojahed, Mahmoud, Khosrow et
tous nos proches qui ont courageusement affronté les criminels de la
République islamique sont en danger.

Il est maintenant de notre devoir d’être leur voix.

Lors de rassemblements mondiaux à l’occasion de l’anniversaire du meurtre
légalisé des enfants de l’Iran, nous nous réunissons pour protester contre
les exécutions et l’oppression des femmes en Iran.

Nous restons unis jusqu’à ce que le jour de la liberté arrive.

Commémoration du premier anniversaire de l’exécution des trois jeunes innocents
par la République islamique d’Iran

"Majid Kazemi, Saeed Yaghoubi, Saleh Mirhashemi"

Samedi 18 mai 2024
15h - 17h
📍place Félix-Poulat, #Grenoble


Quelques photos
==================

.. figure:: images/20240518_151723_800.webp
.. figure:: images/20240518_151730_800.webp
.. figure:: images/20240518_151800_800.webp
.. figure:: images/20240518_151824_800.webp
.. figure:: images/20240518_151843_800.webp
.. figure:: images/20240518_152435_800.webp
.. figure:: images/20240518_153959_800.webp
.. figure:: images/20240518_154119_800.webp
.. figure:: images/20240518_154125_800.webp
.. figure:: images/20240518_154336_800.webp


Vidéos
===========

Discours d'Amnesty Internationale à Grenoble contre la peine de mort
------------------------------------------------------------------------

.. youtube:: 3YX1BYtsrXg


Discours d'une militante de Solidiran à Grenoble contre la peine de mort en Iran
--------------------------------------------------------------------------------------

.. youtube:: TDJlOiRUETQ

Une militante de Solidiran puis intervention de Zoya d'Iran Solidarités pour que cesse le gouvernement islamique d'Iran
--------------------------------------------------------------------------------------------------------------------------

.. youtube:: ZnLgxhciwto


Intervention de Zoya d'Iran Solidarités pour que cesse le gouvernement islamique d'Iran
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/ZnLgxhciwto?t=124
